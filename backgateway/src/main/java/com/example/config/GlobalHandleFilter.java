package com.example.config;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Order(5)
@Component
public class GlobalHandleFilter implements GlobalFilter {
    @SneakyThrows
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String s1 = request.getPath().toString();
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        ArrayList<String> paths = new ArrayList<>();
        paths.add("/employee/login");
        paths.add("/employee/logout");
        paths.add("/common/upload");
        paths.add("/common/upload/**");
        paths.add("/rolePermission/**");
        paths.add("/role/**");
        paths.add("/permission/**");
        for (String path : paths) {
            boolean match = antPathMatcher.match(path, s1);
            if(match){
                return chain.filter(exchange);
            }
        }
        List<String> tokenStr = request.getHeaders().get("token");
        if(Objects.nonNull(tokenStr)){
            String s = tokenStr.toString();
            String token = s.substring(2, s.length() - 2);
                return chain.filter(exchange);
        }
        JSONObject message = new JSONObject();
        message.put("code",0);
        message.put("msg", "NOT");
        message.put("data",null);
        message.put("success",false);
        byte[] bits = message.toJSONString().getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        //指定编码，否则在浏览器中会中文乱码
        response.setStatusCode(HttpStatus.OK);
        response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));

    }
}
