package com.example.config;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtConfig {
    private final static Integer TIME=24*60*60*7*2;
    private final static String KEY="@!!$!%^@#^&#@&#@@@@!@@$@75689rqwrqwrqwtsdagsdgSGG";
    public static String createToken(String id,String username){
        Map<String, Object> map = new HashMap<>(8);
        map.put("id",id);
        map.put("username",username);
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.SECOND,TIME);
        String token = Jwts.builder().setHeader(new HashMap<>())
                .setSubject(id)
                .setExpiration(instance.getTime())
                .setClaims(map)
                .signWith(SignatureAlgorithm.HS256, KEY)
                .compact();
        return token;
    }
    public static Boolean verityToken(String token){
        try {
           Jwts.parser().setSigningKey(KEY).parseClaimsJws(token);
        }catch (Exception e){
            return false;
        }
        return true;
    }
    public static Map<String,Object> getInfo(String token){
        Jwt jwt = Jwts.parser().setSigningKey(KEY).parse(token);
        Map<String,Object> body =(Map<String, Object>) jwt.getBody();
        return body;
    }
}
