package com.example.config.Interceptor;

import com.alibaba.fastjson.JSON;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.utils.Tokenstr;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Map;


@Component
public class TokenFilter extends OncePerRequestFilter{
    @Autowired
    @Qualifier("redis")
    private RedisTemplate redisTemplate;
    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String[] paths = new String[]{"/static/**","/backend/**","/employee/login"};
        String requestURI = request.getRequestURI();
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        for (String path : paths) {
            boolean match = antPathMatcher.match(path, requestURI);
            if(match){
                filterChain.doFilter(request,response);
                return;
            }
        }
        String tokenJson = request.getHeader("token");
        if(!StringUtils.hasText(tokenJson)){
            filterChain.doFilter(request,response);
            return;
        }
        String token = JSON.parseObject(tokenJson,String.class);
        if(!JwtConfig.verityToken(token)){
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.print(JSON.toJSONString(R.error("NOT")));
            return;
        }
        Map<String, Object> map = JwtConfig.getInfo(token);
        String id =(String) map.get("id");
        Collection<? extends GrantedAuthority> loginUser
                =(Collection<? extends GrantedAuthority>) redisTemplate.opsForValue().get("login:"+id);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(id,null,
                        loginUser);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        filterChain.doFilter(request,response);
    }
}
