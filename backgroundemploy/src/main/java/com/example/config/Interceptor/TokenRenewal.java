package com.example.config.Interceptor;


import com.alibaba.fastjson.JSON;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.vo.PermissionValue;
import com.example.service.impl.EmployeeServiceImpl;
import com.example.utils.Tokenstr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
@Order(1000)
public class TokenRenewal implements HandlerInterceptor {
    @Autowired
    @Qualifier("redis")
    private RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private EmployeeServiceImpl employeeService;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//               第一步tokenfilter实质是负责将信息放入securitycontexthold中
//               这个filter负责token刷新和未验证不准登录
        String[] paths = new String[]{"/static/**","/backend/**","/employee/login"};
        String requestURI = request.getRequestURI();
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        for (String path : paths) {
            boolean match = antPathMatcher.match(path, requestURI);
            if(match){
                return true;
            }
        }
        String token = Tokenstr.tokenStr(request);
        if(!StringUtils.hasText(token)){
            ServletOutputStream output= response.getOutputStream();
            String not = JSON.toJSONString(R.error("NOT"));
            output.println(not);
            return false;
        }
//        ThreadLocalUtil.setThreadLocal(token);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String id =(String) map.get("id");
        Boolean existsUser = redisTemplate.hasKey("login:" + id);
        if(!existsUser){
            String username =(String) map.get("username");
            Set<PermissionValue> permissions = employeeService.getPermission(username);
            List<String> authorizes = new ArrayList<>();
            for (PermissionValue permissionValue : permissions) {
                authorizes.add(permissionValue.getPermissionValue());
            }
            List<GrantedAuthority> authorityList =
                    AuthorityUtils.createAuthorityList(authorizes.toArray(new String[authorizes.size()]));
            redisTemplate.opsForValue().set("login:"+id,authorityList,1, TimeUnit.HOURS);
            return true;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
