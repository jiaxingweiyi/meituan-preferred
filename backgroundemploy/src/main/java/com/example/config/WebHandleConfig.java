package com.example.config;

import com.example.config.Interceptor.TokenRenewal;
import com.example.utils.JacksonObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author yeweilin
 */
@Configuration
public class WebHandleConfig implements WebMvcConfigurer {
    @Autowired
    private TokenRenewal renewal;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(renewal)
                .addPathPatterns("/**")
                .excludePathPatterns("/employee/login","/backend/**","/error","backend/plugins/**",
                        "/static/backend/page/**","/static/backend/index.html"
                ,"/static/backend/plugins/axios/axios.min.map");
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        //创建消息转换器对象
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        //设置对象转换器，底层使用Jackson将Java对象转为json
        messageConverter.setObjectMapper(new JacksonObjectMapper());
        //将上面的消息转换器对象追加到mvc框架的转换器集合中
        converters.add(0,messageConverter);
    }
}
