package com.example.config.security;

import com.alibaba.fastjson.JSON;
import com.example.common.R;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class DenyAccessConfig implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setStatus(403);
        response.setCharacterEncoding("utf8");
        response.setContentType("application/json");
        R unAuthentication = R.error("没有权限");
        response.getWriter().write(JSON.toJSONString(unAuthentication));
    }
}
