package com.example.config.security;

import com.alibaba.fastjson.JSON;
import com.example.common.R;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class EntryPointConfig implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(404);
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        R unLogin = R.error("请先登录");
        response.getWriter().write(JSON.toJSONString(unLogin));
    }
}
