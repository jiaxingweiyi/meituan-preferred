package com.example.config.security;

import com.example.entity.Employee;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class LoginUser implements UserDetails {
    private Employee employee;
    private String[] acls;
    private List<GrantedAuthority> authorities;


    public  LoginUser(Employee employee, List<GrantedAuthority> authorities){
        this.employee=employee;
        this.authorities=authorities;
    }
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    @Override
    public String toString() {
        return "LoginUser{" +
                "employee=" + employee +
                ", acls=" + Arrays.toString(acls) +
                '}';
    }

    public String[] getAcls() {
        return acls;
    }

    public void setAcls(String[] acls) {
        this.acls = acls;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return employee.getPassword();
    }

    @Override
    public String getUsername() {
        return employee.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
      return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
