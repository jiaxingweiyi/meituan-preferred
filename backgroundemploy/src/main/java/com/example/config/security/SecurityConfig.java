package com.example.config.security;

import com.example.config.Interceptor.TokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * @author yeweilin
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    @Autowired
    private EntryPointConfig entryPointConfig;
    @Autowired
    private DenyAccessConfig denyAccessConfig;
    @Autowired
    private TokenFilter tokenFilter;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
           http.authorizeRequests()
                   .antMatchers("/nacos/**","/employee/logout","/employee/login",
                           "/static/backend/**","/static/backend/index.html").permitAll()
                   .antMatchers("/employee/login").anonymous()
                   .anyRequest().authenticated();
           http.cors();
           http.csrf().disable();
           http.exceptionHandling().authenticationEntryPoint(entryPointConfig)
                   .accessDeniedHandler(denyAccessConfig);
           http.addFilterBefore(tokenFilter,UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
      web.ignoring().anyRequest();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
