package com.example.config.security;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.example.entity.Employee;
import com.example.entity.vo.PermissionValue;
import com.example.exception.DefineException;
import com.example.service.impl.EmployeeServiceImpl;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Configuration
public class UserDetailServiceConfig implements UserDetailsService {
    @Autowired
    private EmployeeServiceImpl employeeService;
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      if(StrUtil.isBlankIfStr(username)){
          throw new DefineException("账号不能为空");
      }
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        Employee employ = employeeService.getOne(wrapper);
        if(Objects.isNull(employ)){
            throw  new DefineException("不存在这个账号");
        }
        Set<PermissionValue> permissions = employeeService.getPermission(username);
//        if(permissions.isEmpty()){
//            throw  new DefineException("没有权限");
//        }
        List<String> list = new ArrayList<>();
        for (PermissionValue permission : permissions) {
            list.add(permission.getPermissionValue());
        }
        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(list.toArray(new String[list.size()]));
        return new LoginUser(employ,authorityList);
    }
}
