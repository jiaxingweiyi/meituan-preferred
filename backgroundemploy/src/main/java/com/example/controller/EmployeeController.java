package com.example.controller;





/**
 * <p>
 * 员工信息 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */


import com.example.common.R;
import com.example.entity.dto.DtoEmployee;
import com.example.entity.vo.LoginFrom;
import com.example.service.impl.EmployeeServiceImpl;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeServiceImpl employeeService;
    @PostMapping("/login")
    public R<String> toLogin(@RequestBody LoginFrom loginFrom){
        R<String> login = employeeService.login(loginFrom);
        return login;
    }
    @PostMapping("/logout")
    public R<String> toLogout(){
        return employeeService.logout();
    }


    @SneakyThrows
    @PostMapping
    public R<String> addEmploy(@RequestBody DtoEmployee dtoEmployee){
       return employeeService.addEmploy(dtoEmployee);
    }

    @GetMapping("/page")
    public R<Map<String,Object>> paginationEmp(@RequestParam("page") Long page,
                                               @RequestParam("pageSize") Long pageSize,
                                               @RequestParam(value = "name",required = false)String name){
        return employeeService.paginationEmploys(page,pageSize,name);
    }
    @PutMapping
    public R<String> changeEmployStatus(@RequestBody DtoEmployee employee){
        return employeeService.changeStatus(employee);
    }
    @GetMapping("/{id}")
    public R<String> employByid(@PathVariable("id")String id){
        return employeeService.employId(id);
    }
}

