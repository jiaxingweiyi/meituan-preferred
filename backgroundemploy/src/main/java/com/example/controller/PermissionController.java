package com.example.controller;


import com.example.common.R;
import com.example.service.impl.PermissionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 权限 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    private PermissionServiceImpl permissionService;
      @DeleteMapping("/{empId}")
    public R<String> delPermission(@PathVariable("empId")String empId){
          return permissionService.delEmpPermission(empId);
      }
}

