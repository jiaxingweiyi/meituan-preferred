package com.example.controller;


import com.example.common.R;
import com.example.entity.dto.DtoRole;
import com.example.service.impl.RoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/role")
public class RoleController {
   @Autowired
    private RoleServiceImpl roleService;
   @PostMapping("/{empId}")
    public R<String> addRole(@RequestBody DtoRole dtoRole,@PathVariable("empId")String empId){
       return roleService.addRole(dtoRole,empId);
   }
}

