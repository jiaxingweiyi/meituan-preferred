package com.example.controller;


import com.example.common.R;
import com.example.entity.Permission;
import com.example.entity.RolePermission;
import com.example.service.impl.PermissionServiceImpl;
import com.example.service.impl.RolePermissionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色权限 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/rolePermission")
public class RolePermissionController {
    @Autowired
    private PermissionServiceImpl permissionService;
    @Autowired
    private RolePermissionServiceImpl rolePermissionService;
    @GetMapping
    public R<List<Permission>> permissionList(){
     return  R.success(permissionService.permissions());
    }
    @PostMapping("/{roleId}/{ids}")
    public R<String> addPermissionRole(@PathVariable("ids")String ids
            ,@PathVariable("roleId") String roleId){
        return rolePermissionService.addRolePermission(ids,roleId);
    }
}

