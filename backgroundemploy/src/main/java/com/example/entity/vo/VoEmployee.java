package com.example.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoEmployee {
    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("状态 0:禁用，1:正常")
    private Integer status;

    @ApiModelProperty("身份证")
    private String idNumber;
}
