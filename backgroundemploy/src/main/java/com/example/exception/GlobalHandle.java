package com.example.exception;

import com.example.common.R;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @author yeweilin
 */

@RestControllerAdvice
@Order(1)
public class GlobalHandle {
    @ExceptionHandler({DefineException.class,Exception.class})
    public R<String> commonRes(Exception e,Throwable a){
        String message = e.getMessage();
        String msg = a.getMessage();
        if(StringUtils.hasText(message)&&message.length()<=30){
            return R.error(message);
        }else {
            return R.error("服务器异常");
        }
    }
}
