package com.example.mapper;

import com.example.entity.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.Role;
import com.example.entity.vo.PermissionValue;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.awt.*;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 员工信息 Mapper 接口
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

    Set<PermissionValue> getPermission(@Param("username")String username);
    List<Role> sel();
}
