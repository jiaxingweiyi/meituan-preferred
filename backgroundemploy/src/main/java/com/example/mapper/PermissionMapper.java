package com.example.mapper;

import com.example.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

}
