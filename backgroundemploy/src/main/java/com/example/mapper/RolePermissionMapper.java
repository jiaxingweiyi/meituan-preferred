package com.example.mapper;

import com.example.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Mapper
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
