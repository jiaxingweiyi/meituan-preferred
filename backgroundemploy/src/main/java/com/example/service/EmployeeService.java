package com.example.service;


import com.example.entity.Employee;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.dto.DtoEmployee;
import com.example.entity.vo.LoginFrom;
import com.example.entity.vo.PermissionValue;
import com.example.common.R;
import com.example.exception.DefineException;


import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 员工信息 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface EmployeeService extends IService<Employee> {

    R<String> login(LoginFrom loginFrom);

    Set<PermissionValue> getPermission(String username);

    R<String> addEmploy(DtoEmployee dtoUser) throws DefineException;

    R<Map<String,Object>> paginationEmploys(Long page,Long pageSize,String name);

    R<String> changeStatus(DtoEmployee employee);

    R<String> employId(String id);

    R<String> logout();
}
