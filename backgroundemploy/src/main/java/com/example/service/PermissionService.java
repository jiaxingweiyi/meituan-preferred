package com.example.service;

import com.example.common.R;
import com.example.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface PermissionService extends IService<Permission> {
    List<Permission> permissions();
    R<String> delEmpPermission(String empId);
}
