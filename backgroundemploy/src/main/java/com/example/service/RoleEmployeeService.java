package com.example.service;

import com.example.common.R;
import com.example.entity.RoleEmployee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface RoleEmployeeService extends IService<RoleEmployee> {
    Boolean addEmpRole(String roleId,String empId);
}
