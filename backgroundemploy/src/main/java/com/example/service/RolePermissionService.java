package com.example.service;

import com.example.common.R;
import com.example.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.dto.DtoRolePermission;

import java.util.List;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface RolePermissionService extends IService<RolePermission> {
  R<String> addRolePermission(String ids, String roleId);
}
