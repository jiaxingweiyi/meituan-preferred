package com.example.service;

import com.example.common.R;
import com.example.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.dto.DtoRole;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface RoleService extends IService<Role> {
      R<String> addRole(DtoRole dtoRole,String empId);
}
