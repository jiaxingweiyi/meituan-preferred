package com.example.service.impl;

import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.PhoneUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.config.security.LoginUser;
import com.example.entity.Employee;
import com.example.entity.dto.DtoEmployee;
import com.example.entity.vo.LoginFrom;
import com.example.entity.vo.PermissionValue;
import com.example.entity.vo.VoEmployee;
import com.example.exception.DefineException;
import com.example.mapper.EmployeeMapper;
import com.example.service.EmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.JwtConfig;
import com.example.common.R;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 员工信息 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    @Qualifier("redis")
    private RedisTemplate<String,Object> redisTemplate;

    @SneakyThrows
    @Override
    public R login(LoginFrom loginFrom) {
        if(!StringUtils.hasText(loginFrom.getUsername())|| !StringUtils.hasText(loginFrom.getPassword())){
            throw new DefineException("用户名和密码不能为空");
        }
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(loginFrom.getUsername(),loginFrom.getPassword());
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        LoginUser loginUser =(LoginUser) authenticate.getPrincipal();
        if(Objects.isNull(loginUser)){
            throw new DefineException("用户不存在");
        }
        Integer status = loginUser.getEmployee().getStatus();
        if(status==0){
            throw new DefineException("账号被锁定");
        }
        if(!passwordEncoder.matches(loginFrom.getPassword(),loginUser.getPassword())){
            throw new DefineException("密码不一致");
        }
        String userId = loginUser.getEmployee().getId();
        String userName = loginUser.getEmployee().getUsername();
        String token = JwtConfig.createToken(userId, userName);
        redisTemplate.opsForValue().set("login:"+userId,loginUser.getAuthorities(),1,TimeUnit.HOURS);
        Map<String, Object> map = new HashMap<>(4);
        map.put("token",token);
        map.put("username",userName);
        return R.success(map);
    }

    @Override
    public Set<PermissionValue> getPermission(String username) {
        Set<PermissionValue> permission = employeeMapper.getPermission(username);
        return permission;
    }
    @Override
    public R<String> addEmploy(DtoEmployee dtoEmployee)  {
        String idCard = dtoEmployee.getIdNumber();
        if(!IdcardUtil.isValidCard18(idCard)){
            throw new DefineException("身份证格式错误");
        }
        String phone = dtoEmployee.getPhone();
        if(!PhoneUtil.isPhone(phone)){
            throw new DefineException("手机格式错误");
        }
        String username = dtoEmployee.getUsername();
        if(username.length()<3&&username.length()>20){
            throw new DefineException("密码长度大于3小于20");
        }
        String password = dtoEmployee.getPassword();
        if(password.length()<6){
            throw new DefineException("密码长度大于5位");
        }
        String rePassword = dtoEmployee.getRePassword();
        if(!password.equals(rePassword)){
            throw new DefineException("密码不一致");
        }
        String encode = passwordEncoder.encode(password);
        dtoEmployee.setPassword(encode);
        Employee employee = new Employee();
        BeanUtils.copyProperties(dtoEmployee,employee);
        boolean save = this.save(employee);
        if(save){
            return R.success("新增成功");
        }else {
            return R.error("新增失败");
        }
    }

    @Override
    public R<Map<String, Object>> paginationEmploys(Long page,Long pageSize,String name) {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.likeRight(StringUtils.hasText(name),"name",name);
        wrapper.select("id","name","username","phone","status","id_number","sex");
        Page<Employee> paginationEmploys =
                employeeMapper.selectPage(new Page<Employee>(page, pageSize), wrapper);
        List<Employee> records = paginationEmploys.getRecords();
        List<VoEmployee> voEmployees = new ArrayList<>();
        for (Employee record : records) {
            VoEmployee voEmployee = new VoEmployee();
            BeanUtils.copyProperties(record,voEmployee);
            voEmployees.add(voEmployee);
        }
        long total = paginationEmploys.getTotal();
        long pages = paginationEmploys.getPages();
        Map<String, Object> map = new HashMap<>(3);
        map.put("total",total);
        map.put("records",voEmployees);
        map.put("pages",pages);
        return R.success(map);
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> changeStatus(DtoEmployee employee) {
        Employee employ = new Employee();
        String idNumber = employee.getIdNumber();
        if(StringUtils.hasText(idNumber)&&!IdcardUtil.isValidCard18(idNumber)){
            return R.error("身份证错误");
        }
        String username = employ.getUsername();
        if(StringUtils.hasText(username)&&username.length()<3&&username.length()>20){
            return R.error("账号长度大于3小于20");
        }
        String phone = employee.getPhone();
        if(StringUtils.hasText(phone)&&!PhoneUtil.isPhone(phone)){
            return R.error("手机号错误");
        }
        String password = employee.getPassword();
        String rePassword = employee.getRePassword();
        if(StringUtils.hasText(rePassword)&&StringUtils.hasText(password)){
            if(password.equals(rePassword)){
                String encode = passwordEncoder.encode(password);
                employee.setPassword(encode);
            }else {
                return R.error("密码不一致");
            }
        }
        BeanUtils.copyProperties(employee,employ);
        boolean updateData = this.updateById(employ);
        if(updateData){
            return R.success("更新成功");
        }else {
            throw new DefineException("服务器异常");
        }
    }

    @Override
    public R<String> employId(String id) {
        Employee employ = this.getById(id);
        VoEmployee voEmployee = new VoEmployee();
        BeanUtils.copyProperties(employ,voEmployee);
        return R.success(voEmployee);
    }

    @Override
    public R<String> logout() {
        String userId =(String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Boolean logout = redisTemplate.delete("login:" + userId);
        if(logout){
            return R.success("退出成功");
        }else {
            return R.error("退出失败");
        }
    }
}
