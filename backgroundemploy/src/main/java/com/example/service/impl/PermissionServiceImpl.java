package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.example.common.R;
import com.example.entity.Permission;

import com.example.entity.RoleEmployee;
import com.example.entity.RolePermission;
import com.example.exception.DefineException;
import com.example.mapper.PermissionMapper;
import com.example.mapper.RoleEmployeeMapper;
import com.example.mapper.RoleMapper;
import com.example.mapper.RolePermissionMapper;
import com.example.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {
    @Autowired
    private RoleEmployeeMapper roleEmployeeMapper;
    @Autowired
    private RoleServiceImpl roleService;
    @Autowired
    private RolePermissionServiceImpl rolePermissionService;

    @Override
    public List<Permission> permissions() {
        String pidRoot="0";
//        查询所有菜单
        List<Permission> allPermissions = this.list();
//        查询根节点
        List<Permission> rootPermission = new ArrayList<>();
        for (Permission allPermission : allPermissions) {
            if(allPermission.getPid().equals(pidRoot)){
                allPermission.setLevel(1);
                rootPermission.add(allPermission);
            }
        }
        for (Permission permission : rootPermission) {
            List<Permission> childPermission = getChild(permission.getLevel(),permission.getId(),allPermissions);
            //给根节点设置子节点
            permission.setChildren(childPermission);
        }
          return rootPermission;
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> delEmpPermission(String empId) {
        QueryWrapper<RoleEmployee> roleEmployeeQueryWrapper = new QueryWrapper<>();
        roleEmployeeQueryWrapper.eq("employ_id",empId);
        roleEmployeeQueryWrapper.eq("is_deleted",false);
        List<RoleEmployee> roleEmployees = roleEmployeeMapper.selectList(roleEmployeeQueryWrapper);
        List<String> roleIds = new ArrayList<>();
        for (RoleEmployee roleEmployee : roleEmployees) {
            String roleId = roleEmployee.getRoleId();
            roleIds.add(roleId);
        }
        QueryWrapper<RolePermission> rolePermissionQueryWrapper = new QueryWrapper<>();
        for (String roleId : roleIds) {
            rolePermissionQueryWrapper.eq("role_id",roleId);
            rolePermissionQueryWrapper.eq("is_deleted",false);
            boolean remove = rolePermissionService.remove(rolePermissionQueryWrapper);
//            if(!remove){
//                throw new DefineException("取消用户权限失败");
//            }
        }
        int delEmpRole = roleEmployeeMapper.delete(roleEmployeeQueryWrapper);
        if(delEmpRole<=0){
            throw new DefineException("取消用户权限失败");
        }
        boolean removeRole = roleService.removeBatchByIds(roleIds);
        if(!removeRole){
            throw new DefineException("取消用户权限失败");
        }
        return R.success("取消用户权限成功");
    }

    public List<Permission> getChild(Integer level,String id, List<Permission> allPermission){
        List<Permission> childList = new ArrayList<>();
        for (Permission permission : allPermission) {
            // 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
            //相等说明：为该根节点的子节点。
            if (permission.getPid().equals(id)) {
                permission.setLevel(++level);
                childList.add(permission);
            }
        }
        if(childList.size()==0){
            return new ArrayList<>();
        }
        //递归
        for (Permission permission : childList) {
            permission.setChildren(getChild(permission.getLevel(),permission.getId(),allPermission));
        }
        return childList;
    }
}
