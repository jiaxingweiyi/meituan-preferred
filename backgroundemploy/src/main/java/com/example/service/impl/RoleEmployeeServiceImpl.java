package com.example.service.impl;

import com.example.common.R;
import com.example.entity.RoleEmployee;
import com.example.mapper.RoleEmployeeMapper;
import com.example.service.RoleEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class RoleEmployeeServiceImpl extends ServiceImpl<RoleEmployeeMapper, RoleEmployee> implements RoleEmployeeService {

    @Override
    public Boolean addEmpRole(String roleId, String empId) {
        RoleEmployee roleEmployee = new RoleEmployee();
        roleEmployee.setEmployId(empId);
        roleEmployee.setRoleId(roleId);
        boolean save = this.save(roleEmployee);
        if(save){
            return true;
        }else {
            return false;
        }
    }
}
