package com.example.service.impl;

import com.example.common.R;
import com.example.entity.RolePermission;
import com.example.entity.dto.DtoRolePermission;
import com.example.mapper.RolePermissionMapper;
import com.example.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    @Override
    public R<String> addRolePermission(String ids, String roleId) {
        List<RolePermission> rolePermissions = new ArrayList<>();
        String[] permissions = ids.split(",");
        for (String permission : permissions) {
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermissionId(permission);
            rolePermissions.add(rolePermission);
        }
        boolean b = this.saveBatch(rolePermissions);
        if(b){
            return R.success("分配权限成功");
        }else {
            return R.error("分配权限失败");
        }
    }
}
