package com.example.service.impl;

import com.example.common.R;
import com.example.entity.Role;
import com.example.entity.dto.DtoRole;
import com.example.mapper.RoleMapper;
import com.example.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleEmployeeServiceImpl roleEmployeeService;
    @Override
    public R<String> addRole(DtoRole dtoRole,String empId) {
        Role role = new Role();
        BeanUtils.copyProperties(dtoRole,role);
        boolean save = this.save(role);
        if(save){
            String roleId = role.getId();
            Boolean addRoleEmp = roleEmployeeService.addEmpRole(roleId,empId);
            if(addRoleEmp){
                return R.success(roleId);
            }
        }
            return R.error("添加角色失败");
    }
}
