package com.example;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@MapperScan(basePackages = {"com.example.mapper"})
@EnableDiscoveryClient
public class BackgroundmenusortApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackgroundmenusortApplication.class, args);
    }

}
