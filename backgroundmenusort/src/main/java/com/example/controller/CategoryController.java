package com.example.controller;


import com.example.common.R;
import com.example.entity.dto.DtoCategory;
import com.example.entity.vo.VoCategory;
import com.example.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 菜品及套餐分类 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/category")
public class CategoryController {
     @Autowired
     private CategoryServiceImpl categoryService;
     @GetMapping("/page")
     public R<Map<String, Object>> paginationCategory(@RequestParam("page") Long page,
                                                      @RequestParam("pageSize") Long pageSize){
         return categoryService.categoryPagination(page,pageSize);
     }
    @PostMapping
    public R<String> addCategory(@RequestBody VoCategory voCategory){
        return categoryService.addCategory(voCategory);
    }

    @DeleteMapping
    public R<String> delCategory(@RequestParam("ids")String id){
         return categoryService.deleteCategory(id);
    }

    @PutMapping
    public R<String> updateCategory(@RequestBody DtoCategory dtoCategory){
         return categoryService.modifycategory(dtoCategory);
    }
    @GetMapping("/list")
    public R<String> categoryList(@RequestParam("type")Integer type){
         return categoryService.categorys(type);
    }
}

