package com.example.controller;


import com.example.common.R;
import com.example.entity.Dish;
import com.example.entity.DishFlavor;
import com.example.entity.dto.DtoDish;
import com.example.entity.dto.DtoDishSetmealId;
import com.example.entity.vo.VoDish;
import com.example.entity.vo.VoDishSetmeal;
import com.example.entity.vo.VoFrontCategory;
import com.example.service.impl.DishServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜品管理 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/dish")
public class DishController {
    @Autowired
    private DishServiceImpl dishService;
    @GetMapping("/page")
    public R<Map<String,Object>> dishPage(@RequestParam("page") Long page,
                                          @RequestParam("pageSize")Long pageSize,
                                          @RequestParam(value = "name",required = false)String name){
        return dishService.dishPagination(page,pageSize,name);
    }
    @GetMapping("/{id}")
    public R<DtoDish> DishById(@PathVariable("id")String id){
       return dishService.byIdDish(id);
    }
    @PostMapping
    public R<String> insertDish(@RequestBody DtoDish dtoDish){
        return dishService.addDish(dtoDish);
    }
    @DeleteMapping
    public R<String> delDish(@RequestParam("ids")String id){
        return dishService.delDish(id);
    }
    @PutMapping
    public R<String> updateDish(@RequestBody DtoDish dtoDish){
        return dishService.updateDish(dtoDish);
    }
    @PostMapping("/status/{status}")
    public R<String> changeStatus(@PathVariable("status")Integer status,@RequestParam("ids")String id){
        String[] ids = id.split(",");
        return dishService.changeStatus(ids,status);
    }
    @GetMapping("/list")
    public R<List<VoDish>> dishList(@RequestParam("categoryId")String categoryId){
       return dishService.voDishList(categoryId);
    }
    @GetMapping("/frontlist")
    public List<VoFrontCategory> frontMenuList(){
        return dishService.frontMenuList();
    }
    @GetMapping("/frontlist/{id}/{status}")
    public List<VoDishSetmeal> frontDishes(@PathVariable("id")String id
            , @PathVariable("status")Integer status){
        return dishService.Frontdishes(id,status);
    }
    @GetMapping("/frontlist/{name}")
    public DtoDishSetmealId dtoDishSetmealId(@PathVariable("name")String name){
        return dishService.getDishOrSetmealId(name);
    }

}

