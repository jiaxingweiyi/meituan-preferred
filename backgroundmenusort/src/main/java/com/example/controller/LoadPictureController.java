package com.example.controller;

import com.example.common.R;
import com.example.service.impl.FileUpload;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.PublicKey;

/**
 * @author yeweilin
 */
@RestController
@RequestMapping("/common")
public class LoadPictureController {
    @Autowired
    private FileUpload fileUpload;
    @PostMapping("/upload")
    public R<String> uploadImage(@RequestPart("file") MultipartFile multipartFile){
        return fileUpload.fileUpload(multipartFile);
    }
    @DeleteMapping("/upload/{name}")
    public R<String> delUpload(@PathVariable("name")String name){
        return fileUpload.filedel(name);
    }
}
