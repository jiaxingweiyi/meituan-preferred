package com.example.controller;


import com.example.common.R;
import com.example.entity.dto.DtoSeteal;
import com.example.service.impl.SetmealServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 套餐 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {
     @Autowired
     private SetmealServiceImpl service;
    @PostMapping
    public R<String> addSetmeal(@RequestBody DtoSeteal dtoSeteal){
        return service.addSermeal(dtoSeteal);
    }
    @GetMapping("/page")
    public R<Map<String,Object>> setmealPagination(@RequestParam("page")Long page,
                                                   @RequestParam("pageSize")Long pageSize,
                                                   @RequestParam(value = "name",required = false)String name){
         return service.setmealPagination(page,pageSize,name);
    }
    @PostMapping("/status/{status}")
    public R<String> changeSetmealStatus(@RequestParam("ids")String id,@PathVariable("status")Integer status){
        return service.changeStatus(id,status);
    }
    @DeleteMapping
    public R<String> delSetmeal(@RequestParam("ids") String id){
        return service.delSermeal(id);
    }

    @GetMapping("{id}")
    public R<DtoSeteal> IdSetmeal(@PathVariable("id")String id){
        return service.byIdSermeal(id);
    }
    @PutMapping
    public R<String> modifiedSetmeal(@RequestBody DtoSeteal dtoSeteal){
        return service.modifiedSermeal(dtoSeteal);
    }
}

