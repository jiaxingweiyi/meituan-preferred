package com.example.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoCategory {
    @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("分类名称")
    private String name;
    @ApiModelProperty("顺序")
    private Integer sort;
}
