package com.example.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoDish {
    private String id;

    @ApiModelProperty("菜品名称")
    private String name;

    @ApiModelProperty("菜品分类")
    private  String categoryId;

    @ApiModelProperty("图片")
    private String image;

    @ApiModelProperty("菜品价格")
    private BigDecimal price;

    @ApiModelProperty("菜品码")
    private String code;

    @ApiModelProperty("描述信息")
    private String description;

    @ApiModelProperty("口味名称")
    private List<Map<String,Object>> flavors;

}
