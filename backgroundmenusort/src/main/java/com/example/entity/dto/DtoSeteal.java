package com.example.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoSeteal {
    @ApiModelProperty
    private String id;

    @ApiModelProperty("套餐名称")
    private String name;

    @ApiModelProperty("菜品分类id")
    private String categoryId;

    @ApiModelProperty("套餐价格")
    private BigDecimal price;

    @ApiModelProperty("描述信息")
    private String description;

    @ApiModelProperty("图片")
    private String image;

   @ApiModelProperty("套餐菜品")
    private List<DtoSetmealDish> setmealDishes;
}
