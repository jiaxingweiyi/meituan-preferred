package com.example.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoDish implements Serializable {
    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("菜品名称")
    private String name;

    @ApiModelProperty("菜品分类")
    private  String categoryName;

    @ApiModelProperty("图片")
    private String image;

    @ApiModelProperty("菜品价格")
    private BigDecimal price;

    @ApiModelProperty("0 停售 1 起售")
    private Integer status;

    private Integer type;

    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy:MM:dd HH:mm:ss")
    private LocalDateTime updateTime;

    private Integer isDeleted;

}
