package com.example.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoDishSetmeal {
    private String dishId;
    private String setmealId;
    private String name;
    private String description;
    private String image;
    private String price;
    private Integer count;
    private List<VoFlavor> flavors;
}
