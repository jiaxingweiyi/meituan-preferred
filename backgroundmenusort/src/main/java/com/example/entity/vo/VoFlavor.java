package com.example.entity.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoFlavor {
    private String id;
    private String name;
    private String value;
    private String setmealId;
    private String dishId;
}
