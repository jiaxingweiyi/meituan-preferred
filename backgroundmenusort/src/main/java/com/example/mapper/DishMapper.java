package com.example.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.R;
import com.example.entity.Dish;
import com.example.entity.dto.DtoDishSetmealId;
import com.example.entity.dto.DtoSetmealDish;
import com.example.entity.vo.VoDish;
import com.example.entity.vo.VoDishSetmeal;
import com.example.entity.vo.VoFrontCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜品管理 Mapper 接口
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Mapper
public interface DishMapper extends BaseMapper<Dish> {
   Page<VoDish> menuManager(Page page,@Param("name") String name);
   List<VoFrontCategory> frontMenuList();
   List<VoDishSetmeal> Frontdishes(@Param("id")String id, @Param("status")Integer status);
   List<VoDishSetmeal> FrontdishesTwo(@Param("id")String id, @Param("status")Integer status);
   DtoDishSetmealId getDishOrSetmealId(@Param("name")String name);
}
