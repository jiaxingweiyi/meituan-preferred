package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.Setmeal;
import com.example.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 套餐菜品关系 Mapper 接口
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
