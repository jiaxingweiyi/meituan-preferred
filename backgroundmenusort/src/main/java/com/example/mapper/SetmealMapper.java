package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.R;
import com.example.entity.Setmeal;
import com.example.entity.dto.DtoSeteal;
import com.example.entity.vo.VoSetmeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 套餐 Mapper 接口
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
    Page<VoSetmeal> setmealPagination(Page page, @Param("name")String name);
    DtoSeteal IdSetmeal(@Param("id")String id);
}
