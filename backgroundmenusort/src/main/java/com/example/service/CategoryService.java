package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.Category;
import com.example.entity.dto.DtoCategory;
import com.example.entity.vo.VoCategory;

import java.util.Map;

/**
 * <p>
 * 菜品及套餐分类 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface CategoryService extends IService<Category> {
 R<String> addCategory(VoCategory voCategory);
 R<Map<String,Object>> categoryPagination(Long page,Long pageSize);
 R<String> deleteCategory(String id);
 R<String> modifycategory(DtoCategory dtoCategory);
 R<String> categorys(Integer type);
}
