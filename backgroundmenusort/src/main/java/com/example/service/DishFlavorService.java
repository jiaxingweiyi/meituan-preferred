package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.DishFlavor;

/**
 * <p>
 * 菜品口味关系表 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface DishFlavorService extends IService<DishFlavor> {

}
