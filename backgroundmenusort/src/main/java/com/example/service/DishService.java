package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.Dish;
import com.example.entity.dto.DtoDish;
import com.example.entity.dto.DtoDishSetmealId;
import com.example.entity.vo.VoDish;
import com.example.entity.vo.VoDishSetmeal;
import com.example.entity.vo.VoFrontCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜品管理 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface DishService extends IService<Dish> {
  R<Map<String,Object>> dishPagination(Long page,Long pageSize,String name);
  R<String> addDish(DtoDish dtoDish);
  R<String> delDish(String ids);
  R<DtoDish> byIdDish(String id);
  R<String> updateDish(DtoDish dtoDish);
  R<String> changeStatus(String[] ids,Integer status);
  R<List<VoDish>> voDishList(String categoryId);
  List<VoFrontCategory>  frontMenuList();
  List<VoDishSetmeal> Frontdishes(String id, Integer status);
  R<String> addCart(VoDishSetmeal dishSetmeal);
  DtoDishSetmealId getDishOrSetmealId(String name);
}
