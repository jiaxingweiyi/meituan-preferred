package com.example.service;

import com.example.common.R;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
   R<String> fileUpload(MultipartFile multipartFile);
   R<String> filedel(String name);
}
