package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.SetmealDish;

/**
 * <p>
 * 套餐菜品关系 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface SetmealDishService extends IService<SetmealDish> {

}
