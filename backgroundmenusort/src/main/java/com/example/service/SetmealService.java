package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.Setmeal;
import com.example.entity.dto.DtoSeteal;

import java.util.Map;

/**
 * <p>
 * 套餐 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface SetmealService extends IService<Setmeal> {
   R<String> addSermeal(DtoSeteal dtoSeteal);
   R<Map<String,Object>> setmealPagination(Long page,Long pageSize,String name);
   R<String> changeStatus(String id,Integer status);
   R<String> delSermeal(String id);
   R<DtoSeteal> byIdSermeal(String id);
   R<String> modifiedSermeal(DtoSeteal dtoSeteal);
}
