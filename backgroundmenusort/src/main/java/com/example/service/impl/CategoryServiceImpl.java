package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.R;
import com.example.entity.Category;
import com.example.entity.Dish;
import com.example.entity.Setmeal;
import com.example.entity.dto.DtoCategory;
import com.example.entity.vo.VoCategory;
import com.example.exception.DefineException;
import com.example.mapper.CategoryMapper;
import com.example.mapper.DishMapper;
import com.example.mapper.SetmealMapper;
import com.example.service.CategoryService;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜品及套餐分类 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @SneakyThrows
    @Override
    public R<String> addCategory(VoCategory voCategory) {
        Category category = new Category();
        BeanUtils.copyProperties(voCategory,category);
        boolean save = this.save(category);
        if(save){
            return R.success("添加成功");
        }else {
            throw  new DefineException("添加失败");
        }
    }

    @Override
    public R<Map<String, Object>> categoryPagination(Long page, Long pageSize) {
        QueryWrapper<Category> wrapper = new QueryWrapper<>();
        wrapper.select("id","type","name","sort","update_time","create_time");
        wrapper.orderByAsc("sort","create_time");
        Page<Category> categoryPage = this.page(new Page<Category>(page,pageSize), wrapper);
        Map<String, Object> map = new HashMap<>(3);
        List<Category> records = categoryPage.getRecords();
        List<VoCategory> categories = new ArrayList<>();
        for (Category record : records) {
            VoCategory voCategory = new VoCategory();
            BeanUtils.copyProperties(record,voCategory);
            categories.add(voCategory);
        }
        map.put("records",categories);
        map.put("total",categoryPage.getTotal());
        return R.success(map);
    }

    @SneakyThrows
    @Override
    public R<String> deleteCategory(String id) {
        QueryWrapper<Dish> dishWrapper = new QueryWrapper<>();
        dishWrapper.eq("category_id",id);
        Long dishCount = dishMapper.selectCount(dishWrapper);
        if(dishCount>0){
            throw new DefineException("关联了菜品不能删除");
        }
        QueryWrapper<Setmeal> setmealWrapper = new QueryWrapper<>();
        setmealWrapper.eq("category_id",id);
        Long setmealCount = setmealMapper.selectCount(setmealWrapper);
        if(setmealCount>0){
            throw new DefineException("关联了套餐不能删除");
        }
        boolean b = this.removeById(id);
        if(b){
            return R.success("删除成功");
        }else {
            return R.error("删除失败");
        }
    }

    @Override
    public R<String> modifycategory(DtoCategory dtoCategory) {
        Category category = new Category();
        BeanUtils.copyProperties(dtoCategory,category);
        boolean b = this.updateById(category);
        if(b){
            return R.success("更新成功");
        }else {
            return R.error("更新失败");
        }
    }

    @Override
    public R<String> categorys(Integer type) {
        QueryWrapper<Category> wrapper = new QueryWrapper<>();
        wrapper.eq("type",type);
        wrapper.select("type","name","id");
        List<Category> categoryList = this.list(wrapper);
        List<VoCategory> voCategories = new ArrayList<>();
        for (Category category : categoryList) {
            VoCategory voCategory = new VoCategory();
            BeanUtils.copyProperties(category,voCategory);
            voCategories.add(voCategory);
        }
        return R.success(voCategories);

    }

}
