package com.example.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.R;
import com.example.entity.Dish;
import com.example.entity.DishFlavor;
import com.example.entity.SetmealDish;
import com.example.entity.dto.DtoDish;
import com.example.entity.dto.DtoDishSetmealId;
import com.example.entity.vo.VoDish;
import com.example.entity.vo.VoDishSetmeal;
import com.example.entity.vo.VoFrontCategory;
import com.example.exception.DefineException;
import com.example.mapper.DishFlavorMapper;
import com.example.mapper.DishMapper;
import com.example.service.DishFlavorService;
import com.example.service.DishService;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜品管理 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
      @Autowired
      private DishMapper dishMapper;
      @Autowired
      private DishFlavorServiceImpl dishFlavorService;
      @Autowired
      private SetmealDishServiceImpl service;
    @Autowired
    @Qualifier("redis")
    private RedisTemplate redisTemplate;
    @Override
    public R<Map<String, Object>> dishPagination(Long page, Long pageSize,String name) {
        Page<VoDish> voDishPage = dishMapper.menuManager(new Page<>(page, pageSize),name);
        List<VoDish> record = voDishPage.getRecords();
        List<VoDish> records = record.stream().sorted((s, s1) -> {
            LocalDateTime updateTime = s.getUpdateTime();
            LocalDateTime updateTime1 = s1.getUpdateTime();
            if (updateTime.isAfter(updateTime1)) {
                return -1;
            } else {
                return 1;
            }
        }).collect(Collectors.toList());
        long total = voDishPage.getTotal();
        Map<String, Object> map = new HashMap<>(2);
        map.put("total",total);
        map.put("records",records);
        return R.success(map);
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> addDish(DtoDish dtoDish) {
        String code = dtoDish.getCode();
//        QueryWrapper<Dish> wrapper = new QueryWrapper<>();
//        wrapper.select("code");
//        wrapper.eq("code",code);
//        long codeCount = this.count(wrapper);
//        if(codeCount!=0){
//            throw new DefineException("菜品码不能重复");
//        }
        Dish dish = new Dish();
        BeanUtils.copyProperties(dtoDish,dish);
        int insert = dishMapper.insert(dish);
        if(insert<=0){
            throw new DefineException("不可以新增此类菜品");
        }

        if(Objects.isNull(dtoDish.getFlavors())||dtoDish.getFlavors().size()==0){
            return R.success("新增商品成功");
        }
        String dishId = dish.getId();
        ArrayList<DishFlavor> dishFlavors = new ArrayList<>();
        List<Map<String, Object>> flavors = dtoDish.getFlavors();
        for (Map<String, Object> flavor : flavors) {
            DishFlavor dishFlavor = new DishFlavor();
            dishFlavor.setDishId(dishId);
            String name = (String) flavor.get("name");
            String value =(String) flavor.get("value");
            dishFlavor.setName(name);
            dishFlavor.setValue(value);
            QueryWrapper<DishFlavor> dishFlavorQueryWrapper = new QueryWrapper<>();
            dishFlavorQueryWrapper.eq("name",name);
            dishFlavorQueryWrapper.eq("dish_id",dishId);
            long count = dishFlavorService.count(dishFlavorQueryWrapper);
            if(count>0){
                continue;
            }else {
                dishFlavors.add(dishFlavor);
            }
        }
        boolean b = dishFlavorService.saveBatch(dishFlavors);
        if(!b){
            throw new DefineException("不能新增这种商品");
        }
        return R.success("新增菜品成功");
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> delDish(String id) {
        String[] ids = id.split(",");
        boolean b = this.removeBatchByIds(Arrays.asList(ids));
        QueryWrapper<DishFlavor> dishFlavorQueryWrapper = new QueryWrapper<>();
        for (String idDish : ids) {
            dishFlavorQueryWrapper.eq("dish_id",idDish);
            long count = dishFlavorService.count(dishFlavorQueryWrapper);
            if(count>0){
                boolean remove = dishFlavorService.remove(dishFlavorQueryWrapper);
                if(remove){
                    continue;
                }else {
                    throw new DefineException("删除失败");
                }
            }
        }
        for (String dishId : ids) {
            QueryWrapper<SetmealDish> setmealDishQueryWrapper = new QueryWrapper<>();
            setmealDishQueryWrapper.eq("dish_id",dishId);
            long count = service.count(setmealDishQueryWrapper);
            if(count>0){
                boolean remove = service.remove(setmealDishQueryWrapper);
                if(remove){
                    continue;
                }else {
                    throw new DefineException("删除失败");
                }
            }
        }
        if(!b){
            return R.error("删除失败");
        }else {
            return R.success("删除成功");
        }
    }

    @Override
    public R<DtoDish> byIdDish(String id) {
        DtoDish dtoDish = new DtoDish();
        QueryWrapper<Dish> dishWrapper = new QueryWrapper<>();
        dishWrapper.eq("id",id);
        Dish dish = dishMapper.selectOne(dishWrapper);
        BeanUtils.copyProperties(dish,dtoDish);
        QueryWrapper<DishFlavor> dishFlavorWrapper = new QueryWrapper<>();
        dishFlavorWrapper.eq("dish_id",id);
        List<DishFlavor> dishFlavorList = dishFlavorService.list(dishFlavorWrapper);
        List<Map<String, Object>> maps = new ArrayList<>();
        for (DishFlavor dishFlavor : dishFlavorList) {
            Map<String, Object> map = new HashMap<>(2);
            map.put(dishFlavor.getName(),dishFlavor.getValue());
            maps.add(map);
        }
        dtoDish.setFlavors(maps);
        return R.success(dtoDish);
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> updateDish(DtoDish dtoDish) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dtoDish,dish);
        QueryWrapper<Dish> dishQueryWrapper = new QueryWrapper<>();
        dishQueryWrapper.eq("name",dtoDish.getName());
        boolean dishUpdate = this.update(dish, dishQueryWrapper);
        if(!dishUpdate){
            throw new DefineException("更新失败");
        }
        Dish ds = this.getOne(dishQueryWrapper);
        String dishId = ds.getId();
        QueryWrapper<DishFlavor> dishFlavorQueryWrapper = new QueryWrapper<>();
        dishFlavorQueryWrapper.eq("dish_id",dishId);
        List<DishFlavor> list = dishFlavorService.list(dishFlavorQueryWrapper);
        if(list.isEmpty()){
            return R.success("更新成功");
        }
        boolean removeDishFav = dishFlavorService.remove(dishFlavorQueryWrapper);
        if(!removeDishFav){
            throw new DefineException("更新失败");
        }
        List<Map<String, Object>> flavors = dtoDish.getFlavors();
        if(flavors.size()<=0){
            return R.success("更新成功");
        }
        List<DishFlavor> dishFlavors = new ArrayList<>();
        for (Map<String, Object> flavor : flavors) {
            DishFlavor dishFlavor = new DishFlavor();
            String name = (String) flavor.get("name");
            String value =(String) flavor.get("value");
            dishFlavor.setDishId(dishId);
            dishFlavor.setValue(value);
            dishFlavor.setName(name);
            dishFlavors.add(dishFlavor);
        }
        boolean b = dishFlavorService.saveBatch(dishFlavors);
        if(!b){
            throw new DefineException("更新失败");
        }
        return R.success("更新成功");
    }

    @Override
    public R<String> changeStatus(String[] ids,Integer status) {
        List<Dish> dishes = new ArrayList<>();
        for (String id : ids) {
            Dish dish = new Dish();
            dish.setId(id);
            dish.setStatus(status);
           dishes.add(dish);
        }
        boolean b = this.updateBatchById(dishes);
        if(b){
            return R.success("操作成功");
        }else {
            return R.error("操作失败");
        }
    }

    @Override
    public R<List<VoDish>> voDishList(String categoryId) {
        QueryWrapper<Dish> dishQueryWrapper = new QueryWrapper<>();
        dishQueryWrapper.eq("category_id",categoryId);
        List<Dish> dishList = this.list(dishQueryWrapper);
        ArrayList<VoDish> voDishes = new ArrayList<>();
        for (Dish dish : dishList) {
            VoDish voDish = new VoDish();
            BeanUtils.copyProperties(dish,voDish);
            voDishes.add(voDish);
        }
        return R.success(voDishes);
    }

    //  前台List菜单需要的方法
    @SneakyThrows
    @Override
    public List<VoFrontCategory> frontMenuList() {
        List<VoFrontCategory> voFrontCategories =null;
        Boolean menu = redisTemplate.hasKey("menu:list");
        if(menu){
             voFrontCategories =
                    (List<VoFrontCategory>) redisTemplate.opsForValue().get("menu:list");
            return voFrontCategories;
        }
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("menu:lock", "", 30, TimeUnit.SECONDS);
        try {
            while (!lock){
                TimeUnit.MILLISECONDS.sleep(300);
                this.frontMenuList();
            }
             voFrontCategories = dishMapper.frontMenuList();
            if(voFrontCategories.isEmpty()){
                throw new DefineException("没有菜品");
            }
            redisTemplate.opsForValue().set("menu:list",voFrontCategories,1500+ RandomUtil.randomInt(300)
                    , TimeUnit.SECONDS);
        }finally {
            redisTemplate.delete("menu:lock");
        }
        return voFrontCategories;
    }

    @SneakyThrows
    @Override
    public List<VoDishSetmeal> Frontdishes(String id, Integer status) {
        //    不小心写成大写留个眼
        List<VoDishSetmeal> frontdishes=null;
        Boolean dish = redisTemplate.hasKey("dishes:list:"+id);
        if(dish){
            frontdishes  =(List<VoDishSetmeal>) redisTemplate.opsForValue().get("dishes:list:"+id);
            return frontdishes;
        }
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("dish:lock:"+id, "", 30, TimeUnit.SECONDS);
        try {
            while (!lock){
                TimeUnit.MILLISECONDS.sleep(500);
                this.Frontdishes(id,status);
            }
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("category_id",id);
            String code=null;
            List<Dish> listDish = dishMapper.selectList(queryWrapper);
            for (Dish o : listDish) {
                code = o.getCode();
                break;
            }
            if("0".equals(code)){
                frontdishes = dishMapper.Frontdishes(id, status);
            }else {
                frontdishes=dishMapper.FrontdishesTwo(id,status);
            }
            if(Objects.isNull(frontdishes)){
                redisTemplate.opsForValue().set("dishes:list:"+id,"",60,TimeUnit.SECONDS);
                throw new DefineException("没有这个菜品信息");
            }
            redisTemplate.opsForValue().set("dishes:list:"+id,frontdishes,1000+RandomUtil.randomInt(1000)
                    ,TimeUnit.SECONDS);
        }finally {
            redisTemplate.delete("dish:lock:"+id);
        }
        return frontdishes;
    }


    @Override
    public R<String> addCart(VoDishSetmeal dishSetmeal) {
        return null;
    }

    @Override
    public DtoDishSetmealId getDishOrSetmealId(String name) {
        return dishMapper.getDishOrSetmealId(name);
    }
}
