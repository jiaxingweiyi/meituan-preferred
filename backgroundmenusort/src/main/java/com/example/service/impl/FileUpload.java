package com.example.service.impl;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.ObjectMetadata;
import com.example.common.R;
import com.example.exception.DefineException;
import com.example.service.FileUploadService;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.InputStream;

@Service
public class FileUpload implements FileUploadService {
    private final static String accesskeyId = "LTAI5t7hqxaFjdrLQYUjfAWP";
    private final static String endpoint = "oss-cn-beijing.aliyuncs.com";
    private final static String accesskeySecret = "Dp5xpK0h8Y3pveHD3K6LW24ePiCdtp";
    private final static String buketName = "ed-bucket";

    @Override
    public R<String> fileUpload(MultipartFile multipartFile){
        String originalFilename = multipartFile.getOriginalFilename();
        OSS ossClient = new OSSClientBuilder().build(endpoint, accesskeyId, accesskeySecret);
        try {
            InputStream inputStream = multipartFile.getInputStream();
            ObjectMetadata objectMetadata = new ObjectMetadata();
            CreateBucketRequest createBucketRequest = new CreateBucketRequest(null);
            createBucketRequest.setBucketName(buketName);
            createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
            ossClient.createBucket(createBucketRequest);
            ossClient.putObject(buketName, originalFilename, inputStream, objectMetadata);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            ossClient.shutdown();
        }
        //可以访问的图片连接
        return R.success("https://ed-bucket.oss-cn-beijing.aliyuncs.com/"+originalFilename);
    }

    @SneakyThrows
    @Override
    public R<String> filedel(String name){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accesskeyId, accesskeySecret);
        try {
            ossClient.deleteObject(buketName,name);
        }catch (ClientException ce){
            throw new DefineException("删除失败");
        }finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
           return R.success("删除成功");
    }
}
