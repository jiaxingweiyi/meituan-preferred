package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.SetmealDish;
import com.example.mapper.SetmealDishMapper;
import com.example.service.SetmealDishService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 套餐菜品关系 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {

}
