package com.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.R;
import com.example.entity.Setmeal;
import com.example.entity.SetmealDish;
import com.example.entity.dto.DtoSeteal;
import com.example.entity.dto.DtoSetmealDish;
import com.example.entity.vo.VoSetmeal;
import com.example.exception.DefineException;
import com.example.mapper.SetmealDishMapper;
import com.example.mapper.SetmealMapper;
import com.example.service.SetmealService;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 套餐 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
      @Autowired
      private SetmealDishServiceImpl dishService;
      @Autowired
      private SetmealMapper setmealMapper;
    @SneakyThrows
    @Override
    @Transactional
    public R<String> addSermeal(DtoSeteal dtoSeteal) {
        System.out.println(dtoSeteal.toString());
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(dtoSeteal,setmeal);
        boolean saveSetmeal = this.save(setmeal);
        if(!saveSetmeal){
            throw new DefineException("新增套餐失败");
        }
        String  setmealId = setmeal.getId();
        List<DtoSetmealDish> setmealDishes = dtoSeteal.getSetmealDishes();
        List<SetmealDish> setmeals = new ArrayList<>();
        for (DtoSetmealDish setmealDish : setmealDishes) {
            SetmealDish mealDsih = new SetmealDish();
            mealDsih.setSetmealId(setmealId);
            BeanUtils.copyProperties(setmealDish,mealDsih);
            setmeals.add(mealDsih);
        }
        boolean b = dishService.saveBatch(setmeals);
        if(!b){
            throw new DefineException("新增套餐失败");
        }
        return R.success("新增套餐成功");
    }

    @Override
    public R<Map<String, Object>> setmealPagination(Long page, Long pageSize, String name) {
        Page<VoSetmeal> voSetmealPage = setmealMapper.setmealPagination(new Page<VoSetmeal>(page,pageSize),name);
        List<VoSetmeal> record = voSetmealPage.getRecords();
        List<VoSetmeal> records = record.stream().sorted((s, s1) -> {
            LocalDateTime updateTime = s.getUpdateTime();
            LocalDateTime updateTime1 = s1.getUpdateTime();
            if (updateTime.isAfter(updateTime1)) {
                return -1;
            } else {
                return 1;
            }
        }).collect(Collectors.toList());
        long total = voSetmealPage.getTotal();
        Map<String, Object> map = new HashMap<>(2);
        map.put("total",total);
        map.put("records",records);
        return R.success(map);
    }

    @Override
    public R<String> changeStatus(String id,Integer status) {
        String[] ids = id.split(",");
        List<Setmeal> setmeals = new ArrayList<>();
        for (String idStr : ids) {
            Setmeal setmeal = new Setmeal();
            setmeal.setStatus(status);
            setmeal.setId(idStr);
            setmeals.add(setmeal);
        }
        System.out.println(setmeals.toString());
        boolean b = this.updateBatchById(setmeals);
        if(b){
            return R.success("改变状态成功");
        }else {
            return R.error("改变状态失败");
        }
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> delSermeal(String id) {
        String[] ids = id.split(",");
        System.out.println(ids.toString());
        boolean b = this.removeBatchByIds(Arrays.asList(ids));
        for (String s : ids) {
            QueryWrapper<SetmealDish> wrapper = new QueryWrapper<>();
            wrapper.eq("setmeal_id",s);
            boolean remove = dishService.remove(wrapper);
            if(remove){
                continue;
            }else {
                throw new DefineException("删除失败");
            }
        }
        if(b){
            return R.success("删除成功");
        }else {
            throw new DefineException("删除失败");
        }
    }


    @Override
    public R<DtoSeteal> byIdSermeal(String id) {
        System.out.println(id);
        DtoSeteal dtoSeteal = setmealMapper.IdSetmeal(id);
        System.out.println(dtoSeteal);
        if(Objects.isNull(dtoSeteal)){
            return  R.error("服务器异常");
        }else {
            return  R.success(dtoSeteal);
        }
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> modifiedSermeal(DtoSeteal dtoSeteal) {
        System.out.println(dtoSeteal.toString());
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(dtoSeteal,setmeal);
        QueryWrapper<Setmeal> setmealQueryWrapper = new QueryWrapper<>();
        setmealQueryWrapper.eq("id",dtoSeteal.getId());
        boolean updateSermeal = this.update(setmeal, setmealQueryWrapper);
        if(!updateSermeal){
            throw new DefineException("不能这么修改");
        }
        Setmeal one = this.getOne(setmealQueryWrapper);
        String setmealId = one.getId();
        QueryWrapper<SetmealDish> dishQueryWrapper = new QueryWrapper<>();
        dishQueryWrapper.eq("setmeal_id",setmealId);
        boolean removeDish = dishService.remove(dishQueryWrapper);
        if(!removeDish){
            throw new DefineException("修改错误");
        }
        List<DtoSetmealDish> setmealDishes = dtoSeteal.getSetmealDishes();
        List<SetmealDish> setmeals = new ArrayList<>();
        for (DtoSetmealDish setmealDish : setmealDishes) {
            SetmealDish dish = new SetmealDish();
            BeanUtils.copyProperties(setmealDish,dish);
            dish.setSetmealId(setmealId);
            setmeals.add(dish);
        }
        boolean b = dishService.saveBatch(setmeals);
        if(!b){
            throw new DefineException("不能这么操作");
        }
        return R.success("更新成功");
    }
}
