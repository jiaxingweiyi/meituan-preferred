package com.example;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.R;
import com.example.entity.dto.DtoDishSetmealId;
import com.example.entity.dto.DtoSeteal;
import com.example.entity.vo.VoDish;
import com.example.entity.vo.VoDishSetmeal;
import com.example.entity.vo.VoFrontCategory;
import com.example.mapper.DishMapper;
import com.example.mapper.SetmealMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class BackgroundmenusortApplicationTests {
   @Autowired
   private DishMapper dishMapper;
    @Test
    void contextLoads() {
        DtoDishSetmealId z= dishMapper.getDishOrSetmealId("珍珠奶茶");
        System.out.println(z.toString());
    }

}
