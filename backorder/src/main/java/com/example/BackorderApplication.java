package com.example;

import com.example.service.order.OrdersService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(clients = OrdersService.class)
public class BackorderApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackorderApplication.class, args);
    }

}
