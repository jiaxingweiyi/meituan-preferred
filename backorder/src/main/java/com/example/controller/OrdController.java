package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.common.R;
import com.example.service.order.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author yeweilin
 */
@RestController
@RequestMapping("/frontorders")
public class OrdController {
    @Autowired
    private OrdersService ordersService;
    @GetMapping
    public R<Map<String,Object>> ordersList(@RequestParam("page")Long page,
                                            @RequestParam("pageSize")Long pageSize,
                                            @RequestParam(value = "number",required = false)String number,
                                            @RequestParam(value = "beginTime",required = false)String beginTime,
                                            @RequestParam(value = "endTime" ,required = false)String endTime){
        String mapStr = ordersService.orderList(page, pageSize,number,beginTime,endTime);
        Map<String,Object> map = JSON.parseObject(mapStr,Map.class);
        return R.success(map);
    }
    @PutMapping("/{id}/{status}")
    public R<String> send(@PathVariable("id")String id,
                          @PathVariable("status")Integer status){
        Boolean res = ordersService.sendStatus(id,status);
        if(res){
            return R.success("发送成功");
        }else {
            return R.error("发送失败");
        }
    }
}
