package com.example.common;

import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtConfig {
    private final static Integer TIME=24*60*60*7;
    private final static String KEY="@GGASGSAQYQNVNXCVSFJHSWKSKlHGETYWT@12512675689rqwrqwrqwtsdagsdgSGG";
    public static String createToken(String id,String username){
        Map<String, Object> map = new HashMap<>(4);
        map.put("id",id);
        map.put("username",username);
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.SECOND,TIME);
        String token = Jwts.builder().setHeader(new HashMap<>(4))
                .setSubject(id)
                .setExpiration(instance.getTime())
                .setClaims(map)
                .signWith(SignatureAlgorithm.HS256, KEY)
                .compact();
        return token;
    }
    public static Boolean verityToken(String token){
        try {
          Jwts.parser().setSigningKey(KEY).parseClaimsJws(token);
        }catch (Exception e){
            return false;
        }
        return true;
    }
    public static Map<String,Object> getInfo(String token){
        Jwt jwt = Jwts.parser().setSigningKey(KEY).parse(token);
        Map<String,Object> body =(Map<String, Object>) jwt.getBody();
        return body;
    }
}
