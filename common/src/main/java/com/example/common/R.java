package com.example.common;

import lombok.Data;

/**
 * @author yeweilin
 */
@Data
public class R<T> {
    private Boolean success;
    private String msg;
    private T data;
    private Integer code;
    public static<T> R success(T data){
        R r = new R();
        r.setSuccess(true);
        r.setCode(1);
        r.setData(data);
        r.setMsg("成功");
        return  r;
    }
    public static<T> R error(String msg){
        R r = new R();
        r.setSuccess(false);
        r.setMsg(msg);
        r.setData(null);
        r.setCode(0);
        return r;
    }
}
