package com.example.common.mp;


import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.common.JwtConfig;
import com.example.utils.Tokenstr;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author yeweilin
 */
@Component
public class FillFiled implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime localDateTime=LocalDateTime.now();
        this.setFieldValByName("createTime",localDateTime,metaObject);
        this.setFieldValByName("updateTime",localDateTime,metaObject);
        ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        try {
            assert requestAttributes != null;
            String token = Tokenstr.tokenStr(requestAttributes.getRequest());
            if(StringUtils.hasText(token)){
                Map<String, Object> map = JwtConfig.getInfo(token);
                String id =(String) map.get("id");
                this.setFieldValByName("createUser",id,metaObject);
                this.setFieldValByName("updateUser",id,metaObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        LocalDateTime localDateTime=LocalDateTime.now();
       this.setFieldValByName("updateTime",localDateTime,metaObject);
        ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        try {
            String token = Tokenstr.tokenStr(requestAttributes.getRequest());
            if(StringUtils.hasText(token)){
                Map<String, Object> map = JwtConfig.getInfo(token);
                String id =(String) map.get("id");
                this.setFieldValByName("updateUser",id,metaObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
