package com.example.config.Interceptor;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@Slf4j
public class AOPConfig {

    @Pointcut("execution(* com.example.controller.*.*(..))")
    public void cut(){}
    @Before(value = "cut()")
    public void before(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < args.length; i++) {
            if(args[i] instanceof String){
                args[i]=args[i].toString().trim();
            }
        }
    }
    @After(value = "cut()")
    public void after(){}
    @AfterThrowing(value = "cut()")
    public void afterThrow(JoinPoint joinPoint){
        Signature signature = joinPoint.getSignature();
        Class declaringType = signature.getDeclaringType();
        String declaringTypeName = signature.getDeclaringTypeName();
        log.error("报错路径"+declaringType+"."+declaringTypeName);
    }
    @SneakyThrows
    @Around(value = "cut()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint){
        Object proceed = null;
            ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = requestAttributes.getRequest();
            proceed = proceedingJoinPoint.proceed();
        return proceed;
    }
    @AfterReturning(value = "cut()",returning = "obj")
    public void afterReturn(Object obj){}
}
