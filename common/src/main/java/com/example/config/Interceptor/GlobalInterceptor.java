package com.example.config.Interceptor;


import com.alibaba.fastjson.JSON;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.utils.ThreadLocalUtil;
import com.example.utils.Tokenstr;
import lombok.SneakyThrows;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Order(value = 2147483647)
@Component
public class GlobalInterceptor extends OncePerRequestFilter {

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        feign和upload都不会携带token
        String requestURI = request.getRequestURI();
        System.out.println(requestURI);
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        String[] paths =new String[]{"/static/**","/employee/login","/employee/logout","/backend/**","/common/**",
        "/front/**","/user/login/**","/user/send/**","/dish/frontlist/**","/addressBook/frontdefault/**"
        ,"/shoppingCart/pay/**","/order/frontpage/**","/user/front/**","/addressBook/front/**","/wx/login","/login",
        "/wx/callBack","/wx/static/**","/user/front/wechat","/order/frontorders/**","/user/front/friend/**",
        "/user/front/friendlist/**","/socket/web/**"};
        for (String path : paths) {
            if(antPathMatcher.match(path,requestURI)){
                filterChain.doFilter(request,response);
                return;
            }
        }
        String uploadRequest = request.getRequestURI();
        AntPathMatcher antUpload = new AntPathMatcher();
        boolean match = antUpload.match("/common/", uploadRequest);
        if(match){
            ThreadLocalUtil.setThreadLocal(request.getHeader("token"));
            filterChain.doFilter(request,response);
            return;
        }
        String token = Tokenstr.tokenStr(request);
        if(!StringUtils.hasText(token)){
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.print(JSON.toJSONString(R.error("NOT")));
            return;
        }
        filterChain.doFilter(request,response);
    }
}
