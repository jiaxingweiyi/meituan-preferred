package com.example.utils;

public class ThreadLocalUtil {
    private static ThreadLocal<String> threadLocal=new ThreadLocal();

    public static void setThreadLocal(String id){
        threadLocal.set(id);
    }
    public static String getThreadlocal(){
        return threadLocal.get();
    }
    public static void removeTheadlocal(){
        threadLocal.remove();
    }
}
