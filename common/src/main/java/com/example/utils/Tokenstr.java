package com.example.utils;

import com.alibaba.fastjson.JSON;
import com.example.common.JwtConfig;
import lombok.SneakyThrows;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class Tokenstr {
    public static String tokenStr(HttpServletRequest request) {
         String tokenStr = request.getHeader("token");
        String token = JSON.parseObject(tokenStr,String.class);
        return token;
    }
    @SneakyThrows
    public static String getId(HttpServletRequest request){
        String token= tokenStr(request);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String id =(String) map.get("id");
        return id;
    }
}
