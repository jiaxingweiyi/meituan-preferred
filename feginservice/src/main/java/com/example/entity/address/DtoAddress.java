package com.example.entity.address;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoAddress {
    private String id;
    private String consignee;
    private String phone;
    private Integer sex;
    private String detail;
    private String label;
    private boolean isDefault;

}
