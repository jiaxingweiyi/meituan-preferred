package com.example.entity.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoFrontCategory {
    private String id;
    private String name;
    private List<VoDishSetmeal> voDishSetmealList;
}
