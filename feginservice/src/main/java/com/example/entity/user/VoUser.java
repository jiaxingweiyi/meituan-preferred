package com.example.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoUser {
    private String id;
    private String phone;
    private String name;
    private String avatar;
    private String sex;
}
