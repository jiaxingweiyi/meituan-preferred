package com.example.service.address;

import com.example.entity.address.DtoAddress;
import com.example.entity.address.VoAddress;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author yeweilin
 */
@FeignClient(name = "address")
public interface AddressService {
    @GetMapping("/addressBook/frontdefault/{id}")
    DtoAddress getDefaultAddress(@PathVariable("id") String id);
    @GetMapping("/addressBook/front/{id}")
    VoAddress byIdAddress(@PathVariable("id")String id);
}
