package com.example.service.menu;

import com.example.entity.menu.DtoDishSetmealId;
import com.example.entity.menu.VoDishSetmeal;
import com.example.entity.menu.VoFrontCategory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Component
@FeignClient(name = "menusort")
public interface MenuService {
    @GetMapping("/dish/frontlist")
    List<VoFrontCategory> frontMenuList();
    @GetMapping("/dish/frontlist/{id}/{status}")
    List<VoDishSetmeal> frontDishes(@PathVariable("id")String id
            , @PathVariable("status")Integer status);
    @GetMapping("/dish/frontlist/{name}")
    DtoDishSetmealId dtoDishSetmealId(@PathVariable("name")String name);
}
