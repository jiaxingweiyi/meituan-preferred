package com.example.service.order;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author yeweilin
 */
@FeignClient(value = "payment")
public interface OrdersService {
    @GetMapping("/order/frontpage")
     String orderList(@RequestParam("page") Long page,
                      @RequestParam("pageSize") Long pageSize,
                      @RequestParam("number")String number,
                      @RequestParam(value = "beginTime",required = false) String beginTime,
                      @RequestParam(value = "endTime",required = false)String endTime);
    @PutMapping("/order/frontorders/{id}/{status}")
     boolean sendStatus(@PathVariable("id")String id
            ,@PathVariable("status")Integer status);
}
