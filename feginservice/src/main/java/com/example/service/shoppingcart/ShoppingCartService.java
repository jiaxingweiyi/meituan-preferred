package com.example.service.shoppingcart;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "menu")
public interface ShoppingCartService {
    @DeleteMapping("/shoppingCart/pay/{id}")
    Boolean removeBuyCar(@PathVariable("id")String id);
}
