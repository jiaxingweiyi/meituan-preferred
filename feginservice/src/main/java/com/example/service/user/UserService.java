package com.example.service.user;

import com.example.entity.user.DtoUser;
import com.example.entity.user.VoUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yeweilin
 */
@FeignClient(name = "login")
public interface UserService {
    @GetMapping("/user/front/{id}")
    VoUser user(@PathVariable("id")String id);
    @PostMapping("/user/front/wechat")
    Boolean wechatLogin(@RequestBody DtoUser dtoUser);
    @GetMapping("/user/front/friend/{currentPhone}")
    List<VoUser> users(@PathVariable("currentPhone") String currentPhone,
                       @RequestParam(value = "phone",required = false)String phone);
    @GetMapping("/user/front/friendlist/{phone}")
    List<VoUser> byPhoneUser(@PathVariable(value = "phone",required = false) String phone);
}
