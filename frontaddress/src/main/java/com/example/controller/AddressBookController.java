package com.example.controller;



import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.common.R;
import com.example.entity.AddressBook;
import com.example.entity.dto.DtoAddress;
import com.example.entity.vo.VoAddress;
import com.example.service.impl.AddressBookServiceImpl;
import com.example.utils.Tokenstr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 地址管理 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {
    @Autowired
    private AddressBookServiceImpl addressBookService;
    @GetMapping("/list")
    public R<List<DtoAddress>> addressBookList(HttpServletRequest request){
        return addressBookService.addressList(request);
    }
    @PostMapping
    public R<String> addAddress(@RequestBody DtoAddress dtoAddress,HttpServletRequest request){
        return addressBookService.addAddress(dtoAddress,request);
    }
    @GetMapping("/{id}")
    public R<DtoAddress> ByIdGetAddress(@PathVariable("id")String id){
        return addressBookService.getAddress(id);
    }
    @PutMapping
    public R<String> updateAddress(@RequestBody DtoAddress dtoAddress){
        return addressBookService.updateAddress(dtoAddress);
    }
    @PutMapping("/default/{id}")
    public R<DtoAddress> setDefaultAddress(@PathVariable("id") String id){
        return addressBookService.defaultAddress(id);
    }
    @GetMapping("/frontdefault/{id}")
    public DtoAddress getDefaultAddress(@PathVariable("id") String id){
        System.out.println(id);
        return addressBookService.getDefaultAddress(id);
    }

    @GetMapping("/front/{id}")
    public VoAddress byIdAddress(@PathVariable("id")String id){
          return addressBookService.byIdAddress(id);
    }

    @GetMapping("/location")
    public R<Map<String,Object>> geoInfo(HttpServletRequest request){
        Map<String, Object> resMap = new HashMap<>(4);
        String userId = Tokenstr.getId(request);
        QueryWrapper<AddressBook> addressBookQueryWrapper = new QueryWrapper<>();
        addressBookQueryWrapper.eq("user_id",userId);
        addressBookQueryWrapper.eq("is_default",1);
        AddressBook addressBook = addressBookService.getOne(addressBookQueryWrapper);
        String detailAddress = addressBook.getDetail();
        String url="https://restapi.amap.com/v3/geocode/geo?address=%s&output=json&key=4858649be06b5d21347857c08346b17b";
        url=String.format(url,detailAddress);
        HttpResponse execute = HttpRequest.get(url).execute();
        JSONObject jsonObject = JSON.parseObject(execute.body());
        JSONArray geocodes = jsonObject.getJSONArray("geocodes");
        Map map =(Map) geocodes.get(0);
        String location =(String) map.get("location");
        String[] split = location.split(",");
           resMap.put("Longitude",split[0]);
           resMap.put("latitude",split[1]);
           resMap.put("detailAddress",detailAddress);
        return R.success(resMap);
    }
}

