package com.example.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoAddress {
    @ApiModelProperty("主键")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    @ApiModelProperty("收货人")
    private String consignee;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("性别 0 女 1 男")
    private Integer sex;
    @ApiModelProperty("详细地址")
    private String detail;
    @ApiModelProperty("标签")
    private String label;
    @ApiModelProperty("默认 0 否 1是")
    private boolean isDefault;

}
