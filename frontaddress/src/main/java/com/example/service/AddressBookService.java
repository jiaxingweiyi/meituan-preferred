package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.AddressBook;
import com.example.entity.dto.DtoAddress;
import com.example.entity.vo.VoAddress;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 地址管理 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface AddressBookService extends IService<AddressBook> {
 R<String> addAddress(DtoAddress dtoAddress, HttpServletRequest request);
 R<List<DtoAddress>> addressList(HttpServletRequest request);
 R<DtoAddress> getAddress(String id);
 R<String> updateAddress(DtoAddress dtoAddress);
 R<DtoAddress> defaultAddress(String id);
 DtoAddress getDefaultAddress(String id);
 VoAddress byIdAddress(String id);
}
