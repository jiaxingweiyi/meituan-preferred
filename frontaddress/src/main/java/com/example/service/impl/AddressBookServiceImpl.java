package com.example.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.common.http.HttpClientBeanHolder;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.AddressBook;
import com.example.entity.dto.DtoAddress;
import com.example.entity.vo.VoAddress;
import com.example.exception.DefineException;
import com.example.mapper.AddressBookMapper;
import com.example.service.AddressBookService;
import com.example.utils.Tokenstr;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 地址管理 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {

    @SneakyThrows
    @Override
    public R<String> addAddress(DtoAddress dtoAddress, HttpServletRequest request) {
        String token = Tokenstr.tokenStr(request);
        Map<String, Object> map= JwtConfig.getInfo(token);
        String id =(String) map.get("id");
        AddressBook addressBook = new AddressBook();
        BeanUtils.copyProperties(dtoAddress,addressBook);
        addressBook.setUserId(id);
        boolean save = this.save(addressBook);
        if(!save){
            return R.error("添加地址失败");
        }else {
            return R.success("添加地址成功");
        }
    }
    @SneakyThrows
    @Override
    public R<List<DtoAddress>> addressList(HttpServletRequest request) {
        String token = Tokenstr.tokenStr(request);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String userId =(String) map.get("id");
        QueryWrapper<AddressBook> addressBookQueryWrapper = new QueryWrapper<>();
        addressBookQueryWrapper.eq("user_id",userId);
        List<AddressBook> addressBooks = this.list(addressBookQueryWrapper);
        ArrayList<DtoAddress> dtoAddresses = new ArrayList<>();
        for (AddressBook addressBook : addressBooks) {
            DtoAddress dtoAddress = new DtoAddress();
            BeanUtils.copyProperties(addressBook,dtoAddress);
            dtoAddresses.add(dtoAddress);
        }
        return R.success(dtoAddresses);
    }

    @Override
    public R<DtoAddress> getAddress(String id) {
        QueryWrapper<AddressBook> addressBookQueryWrapper = new QueryWrapper<>();
        addressBookQueryWrapper.eq("id",id);
        AddressBook address = this.getOne(addressBookQueryWrapper);
        DtoAddress dtoAddress = new DtoAddress();
        BeanUtils.copyProperties(address,dtoAddress);
        return R.success(dtoAddress);
    }

    @Override
    public R<String> updateAddress(DtoAddress dtoAddress) {
        AddressBook addressBook = new AddressBook();
        BeanUtils.copyProperties(dtoAddress,addressBook);
        boolean update = this.update(addressBook,null);
        if(!update){
            return R.error("修改失败");
        }
        return R.success("修改成功");
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<DtoAddress> defaultAddress(String id) {
        ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String tokenStr = requestAttributes.getRequest().getHeader("token");
        String token = JSON.parseObject(tokenStr, String.class);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String userId =(String) map.get("id");
        QueryWrapper<AddressBook> countWrapper = new QueryWrapper<>();
        QueryWrapper<AddressBook> delDefaultAddress = new QueryWrapper<>();
        countWrapper.eq("is_default",true);
        countWrapper.eq("id",id);
        long count = this.count(countWrapper);
        System.out.println(count);
        if(count>0){
            return R.error("已设为默认地址了");
        }
        delDefaultAddress.eq("is_default",true);
        delDefaultAddress.eq("user_id",userId);
        AddressBook addressBook = this.getOne(delDefaultAddress);
        if(Objects.nonNull(addressBook)){
            addressBook.setDefault(false);
            boolean delDefault = this.updateById(addressBook);
            if(!delDefault){
                throw new DefineException("设置默认地址失败");
            }
        }
        QueryWrapper<AddressBook> addressBookQueryWrapper = new QueryWrapper<>();
        addressBookQueryWrapper.eq("id",id);
        addressBookQueryWrapper.eq("is_default",false);
         addressBook = this.getOne(addressBookQueryWrapper);
        addressBook.setDefault(true);
        DtoAddress dtoAddress = new DtoAddress();
        BeanUtils.copyProperties(addressBook,dtoAddress);
        boolean update = this.updateById(addressBook);
        if(!update){
        throw new DefineException("设置默认地址失败");
        }
        return R.success(dtoAddress);
    }

    @Override
    public DtoAddress getDefaultAddress(String id) {
        QueryWrapper<AddressBook> addressWrapper = new QueryWrapper<>();
        addressWrapper.eq("is_default",true);
        addressWrapper.eq("user_id",id);
        AddressBook address= this.getOne(addressWrapper);
        DtoAddress dtoAddress = new DtoAddress();
        BeanUtils.copyProperties(address,dtoAddress);
        return dtoAddress;
    }

    @Override
    public VoAddress byIdAddress(String id) {
        QueryWrapper<AddressBook> addressBookQueryWrapper = new QueryWrapper<>();
        addressBookQueryWrapper.eq("is_default",true);
        addressBookQueryWrapper.eq("user_id",id);
        AddressBook addressBook = this.getOne(addressBookQueryWrapper);
        if(Objects.isNull(addressBook)){
            return null;
        }
        VoAddress voAddress = new VoAddress();
        BeanUtils.copyProperties(addressBook,voAddress);
        return voAddress;
    }
}
