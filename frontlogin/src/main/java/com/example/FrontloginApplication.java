package com.example;

import com.example.service.UserService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.example.mapper"})
@EnableRetry
public class FrontloginApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrontloginApplication.class, args);
    }

}
