package com.example.controller;


import com.example.common.R;
import com.example.entity.user.DtoUser;
import com.example.entity.user.VoUser;
import com.example.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceImpl userService;
    @PostMapping("/login/{phone}/{code}")
    public R<Map<String,Object>> toLogin(@PathVariable("phone") String phone,
                          @PathVariable("code") String code){
       return  userService.toLogin(phone,code);
    }
    @GetMapping("/send/{phone}")
    public R<String> sengMsg(@PathVariable("phone")String phone){
        return userService.sendMsg(phone);
    }

    @PostMapping("/logout/{phone}")
    public R<String> toLogout(@PathVariable("phone")String phone){
       return userService.toLogout(phone);
    }
    @GetMapping("/front/{id}")
    public VoUser user(@PathVariable("id")String id){
        return userService.ByIdUser(id);
    }
    @PostMapping("/front/wechat")
    public Boolean wechatLogin(@RequestBody DtoUser dtoUser){
        return userService.wechatLogin(dtoUser);
    }
    @GetMapping("/front/friend/{currentPhone}")
    public List<VoUser> users(@PathVariable("currentPhone") String currentPhone,
                              @RequestParam(value = "phone",required = false)String phone){
        return userService.userList(currentPhone,phone);
    }
    @GetMapping("/front/friendlist/{phone}")
    public List<VoUser> byPhoneUser(@PathVariable(value = "phone",required = false) String phone){
        return userService.byPhoneUser(phone);
    }
    @GetMapping("/frontUserData/{phone}")
    public R<VoUser> userInfo(@PathVariable(value = "phone",required = false) String phone){
        return userService.byPhoneGetUser(phone);
    }
}
