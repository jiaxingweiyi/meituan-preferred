package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
@ApiModel(value = "员工",description = "员工实体类")
public class User {
     @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("姓名")
     private String name;
    @ApiModelProperty("手机号")
     private String phone;
    @ApiModelProperty("性别")
     private String sex;
    @ApiModelProperty("身份证号")
     private String idNumber;
    @ApiModelProperty("头像")
     private String avatar;
    @ApiModelProperty("状态 0:禁用，1:正常")
     private Integer status;
}
