package com.example.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.User;
import com.example.entity.user.DtoUser;
import com.example.entity.user.VoUser;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface UserService extends IService<User> {
    R<Map<String,Object>> toLogin(String phone, String code);
    R<String> sendMsg(String phone);
    R<String> toLogout(String phone);
    VoUser ByIdUser(String id);
    List<VoUser> byPhoneUser(String phone);
    Boolean wechatLogin(DtoUser dtoUser);
    List<VoUser> userList(String currentPhone,String phone);
    R<VoUser> byPhoneGetUser(String phone);
}
