package com.example.service.impl;

import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.User;
import com.example.entity.user.DtoUser;
import com.example.entity.user.VoUser;
import com.example.exception.DefineException;
import com.example.mapper.UserMapper;
import com.example.service.UserService;
import com.example.utils.Tokenstr;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author yeweilin
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>  implements UserService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    @Qualifier("redis")
    private RedisTemplate redisTemplate;
    @Override
    public R<Map<String,Object>> toLogin(String phone,String code) {
          if(!StringUtils.hasText(phone)&&!StringUtils.hasText(code)){
              return R.error("手机号和验证码不能为空");
          }
        String codeMsg = stringRedisTemplate.opsForValue().get(phone);
        if(!codeMsg.equals(code)){
            return R.error("验证码不一致");
        }
        Map<String, Object> map = new HashMap<>(2);
        map.put("phone",phone);
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
          userQueryWrapper.eq("phone",phone);
        User user = this.getOne(userQueryWrapper);
        String token=null;
        if(Objects.nonNull(user)){
            Integer status = user.getStatus();
            if(status==0){
                return R.error("封号中");
            }
        }else {
            user=new User();
            user.setPhone(phone);
            boolean save = this.save(user);
            if(!save){
                return R.error("验证码错误");
            }
        }
        token = JwtConfig.createToken(user.getId(), user.getPhone());
        map.put("phone",phone);
        map.put("token",token);
        return R.success(map);
    }

    private  Integer  computerTime=0;
    @SneakyThrows
    @Override
    @Retryable(value = RuntimeException.class,maxAttempts = 3,backoff = @Backoff(delay = 1000L,multiplier = 1.5))
    public R<String> sendMsg(String phone) {
        if(computerTime==5){
            throw new DefineException("发送短信失败");
        }
        if(!PhoneUtil.isPhone(phone)){
            throw new DefineException("手机格式错误");
        }
        Boolean existPhone = stringRedisTemplate.hasKey(phone);
        if(existPhone){
            throw new DefineException("已发送过了请过期后重试");
        }
        while (true){
            Integer i = RandomUtil.randomInt(1000, 9999);
            String code = i.toString();
            log.debug("发送验证码:"+code);
            Boolean res = stringRedisTemplate.opsForValue().setIfAbsent(phone, code, 15, TimeUnit.MINUTES);
            if(res){
                this.computerTime=0;
                break;
            }else {
                TimeUnit.MILLISECONDS.sleep(200);
                this.computerTime++;
            }
        }
        return R.success("发送消息成功有效期15分钟");
    }

    @Recover
    public R<String> recover(){
        return R.error("服务器异样");
    }

    @Override
    public R<String> toLogout(String phone) {
        if(stringRedisTemplate.hasKey(phone)){
            Boolean deletePhone = stringRedisTemplate.delete(phone);
            if(deletePhone){
                return R.success("退出成功");
            }else {
                return R.error("退出失败");
            }
        }
        return R.success("退出成功");
    }

    @Override
    public VoUser ByIdUser(String id) {
        User user = this.getById(id);
        VoUser voUser = new VoUser();
        if(voUser!=null){
            BeanUtils.copyProperties(user,voUser);
        }
        return voUser;
    }

    @Override
    public List<VoUser> byPhoneUser(String phone) {
        String[] phones = phone.split(",");
        List<VoUser> voUsers = new ArrayList<>();
        for (String ph : phones) {
            QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
            userQueryWrapper.select("id","phone","avatar");
            userQueryWrapper.eq("phone",ph);
            VoUser voUser = new VoUser();
            User user = this.getOne(userQueryWrapper);
            BeanUtils.copyProperties(user,voUser);
            voUsers.add(voUser);
        }
        return voUsers;
    }

    @Override
    public Boolean wechatLogin(DtoUser dtoUser) {
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        System.out.println(dtoUser.getId());
//        userWrapper.eq
        userWrapper.eq("id",dtoUser.getId());
        User user = this.getOne(userWrapper);
        if(user!=null){
            return true;
        }
         user =new User();
        BeanUtils.copyProperties(dtoUser,user);
        boolean save = this.save(user);
        if(save){
            return true;
        }else {
            return false;
        }
    }

    @SneakyThrows
    @Override
    public List<VoUser> userList(String CurrentUserPhone,String phone) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("status",1);
        ArrayList<VoUser> voUsers = new ArrayList<>();
        userQueryWrapper.select("id","avatar","phone");
        if(StringUtils.hasText(phone)){
            userQueryWrapper.eq("phone",phone);
            User user = this.getOne(userQueryWrapper);
            VoUser voUser = new VoUser();
            BeanUtils.copyProperties(user,voUser);
            voUsers.add(voUser);
            return voUsers;
        }
        userQueryWrapper.orderByAsc("id");
        List<User> list = this.list(userQueryWrapper);
        for (User user : list) {
            if(voUsers.size()==10){
                break;
            }
            Double score = redisTemplate.opsForZSet().score("unfriend:" + user.getPhone(),CurrentUserPhone);
            if(Objects.nonNull(score)){
                continue;
            }
            if(user.getPhone().equals(CurrentUserPhone)){
                continue;
            }
            VoUser voUser = new VoUser();
            BeanUtils.copyProperties(user,voUser);
            voUsers.add(voUser);
        }
        return voUsers;
    }

    @Override
    public R<VoUser> byPhoneGetUser(String phone) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("phone",phone);
        userQueryWrapper.select("id","sex","avatar");
        User user = this.getOne(userQueryWrapper);
        VoUser voUser = new VoUser();
        BeanUtils.copyProperties(user,voUser);
        return R.success(voUser);
    }

}
