package com.example;

import com.example.service.menu.MenuService;
import com.example.service.user.UserService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.example.mapper"})
@EnableFeignClients(clients = {MenuService.class, UserService.class})
public class FrontmenuApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrontmenuApplication.class, args);
    }

}
