package com.example.controller;

import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.Comment;
import com.example.service.impl.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
public class CommentController {

    @Autowired
    private CommentServiceImpl commentService;
    @GetMapping("/comment/{menuId}")
    public R<List<Comment>> comments(@PathVariable("menuId")String menuId,
                                     HttpServletRequest request){
        List<Comment> comments = commentService.byIdSelComment(menuId,request);
        return R.success(comments);
    }
    @PostMapping("/comment")
    public R<String> saveComment(@RequestBody Comment commentObj,
                                 HttpServletRequest request){
        boolean b = commentService.saveComment(commentObj, request);
        if(b){
            return R.success("添加评论成功");
        }else {
            return R.error("添加评论失败");
        }
    }

    @DeleteMapping("/comment/{id}")
    public R<String> deleteComment(@PathVariable("id")String id){
        return commentService.delComment(id);
    }
}
