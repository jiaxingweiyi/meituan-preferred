package com.example.controller;


import com.example.common.R;
import com.example.entity.ShoppingCart;
import com.example.entity.dtoShoppingCart.DtoShoppingCart;
import com.example.entity.menu.DtoDishSetmealId;
import com.example.entity.menu.VoDishSetmeal;
import com.example.entity.menu.VoFrontCategory;
import com.example.entity.vo.VoShoppingCart;
import com.example.service.impl.ShoppingCartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartServiceImpl cartService;
    @GetMapping("/list")
    public R<List<VoFrontCategory>> showMenu(){
        return cartService.menuList();
    }
    @GetMapping("/disheslist")
    public R<List<VoDishSetmeal>> showDishes(@RequestParam("categoryId")String id,
                                             @RequestParam("status")Integer status){
        return cartService.dishList(id,status);
    }
    @PostMapping("/add")
    public R<String> addBuyCar(@RequestBody DtoShoppingCart dtoShoppingCart, HttpServletRequest request){
        return cartService.addBuyCar(dtoShoppingCart,request);
    }
    @GetMapping("/cartlilst")
    public R<VoShoppingCart> shoppingCartList(HttpServletRequest request){
        return cartService.shoppingList(request);
    }
    @PostMapping("/sub")
    public R<Integer> updateBuyCar(@RequestBody DtoShoppingCart dtoShoppingCart,HttpServletRequest request){
        return cartService.subBuyCar(dtoShoppingCart,request);
    }
    @DeleteMapping("/clean")
    public R<String> clearBuyCar(HttpServletRequest request){
        return cartService.clearBuyCar(request);
    }
    @DeleteMapping("/pay/{id}")
    public Boolean removeBuyCar(@PathVariable("id") String userId){
        return cartService.delBuyCar(userId);
    }
}

