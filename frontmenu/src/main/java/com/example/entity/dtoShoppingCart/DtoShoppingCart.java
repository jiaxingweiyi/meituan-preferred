package com.example.entity.dtoShoppingCart;


import com.example.entity.menu.VoFlavor;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoShoppingCart {
    @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("图片")
    private String image;
    @ApiModelProperty("菜品id")
    private String dishId;
    @ApiModelProperty("套餐id")
    private String setmealId;
    @ApiModelProperty("数量")
    private Integer number;
    @ApiModelProperty("金额")
    private BigDecimal amount;
    @ApiModelProperty("口味")
    private String dishFlavor;
}
