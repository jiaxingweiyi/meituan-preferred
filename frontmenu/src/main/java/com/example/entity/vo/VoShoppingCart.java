package com.example.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoShoppingCart {
    private String id;
    private String name;
    private String image;
    private Integer number;
    private BigDecimal amount;
    private String dishId;
    private String setmealId;
    private String dishFlavor;
}
