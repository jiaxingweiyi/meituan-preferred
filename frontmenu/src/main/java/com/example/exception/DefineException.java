package com.example.exception;


/**
 * @author yeweilin
 */

public class DefineException extends RuntimeException{
    private String message;
    public DefineException(String message) {
        super(message);
        this.message=message;
    }
}
