package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.common.R;
import com.example.entity.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentMapper extends BaseMapper<Comment> {
    List<Comment> byIdSelComment(@Param("menuId")String menuId, @Param("id")String id);
    R<String> delComment(@Param("id")String id);
}
