package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.Comment;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CommentService extends IService<Comment> {
    List<Comment> byIdSelComment(String menuId,HttpServletRequest request);
    boolean saveComment(Comment comment, HttpServletRequest request);
    R<String> delComment(String id);
}
