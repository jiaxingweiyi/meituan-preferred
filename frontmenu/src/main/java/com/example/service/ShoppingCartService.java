package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.ShoppingCart;
import com.example.entity.dtoShoppingCart.DtoShoppingCart;
import com.example.entity.menu.DtoDishSetmealId;
import com.example.entity.menu.VoDishSetmeal;
import com.example.entity.menu.VoFrontCategory;
import com.example.entity.vo.VoShoppingCart;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
  R<List<VoFrontCategory>> menuList();
  R<List<VoDishSetmeal>> dishList(String id, Integer status);
  R<String> addBuyCar(DtoShoppingCart dtoShoppingCart, HttpServletRequest request);
  R<VoShoppingCart> shoppingList(HttpServletRequest request);
  R<Integer> subBuyCar(DtoShoppingCart dtoShoppingCart,HttpServletRequest request);
  R<String> clearBuyCar(HttpServletRequest request);
   Boolean  delBuyCar(String userId);
}
