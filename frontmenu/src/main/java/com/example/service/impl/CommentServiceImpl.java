package com.example.service.impl;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.Comment;
import com.example.entity.user.VoUser;
import com.example.mapper.CommentMapper;
import com.example.service.CommentService;
import com.example.service.user.UserService;
import com.example.utils.Tokenstr;
import org.apache.catalina.util.RequestUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author yeweilin
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment>implements CommentService {
    @Autowired
    private CommentMapper commentMapper;
    private static List<String> ids = new ArrayList<>();
    @Autowired
    private UserService userService;
    @Override
    public List<Comment> byIdSelComment(String menuId,HttpServletRequest request){
        // 默认传入-1，找出父级的评论，假装查看博客id为7的评论
        List<Comment> comments = commentMapper.byIdSelComment(menuId,"-1");
        findParent(comments);
        return comments;
    }

    @Override
    public boolean saveComment(Comment comment, HttpServletRequest request) {
        String id = Tokenstr.getId(request);
        VoUser user = userService.user(id);
        comment.setAvatar(user.getAvatar());
        comment.setNickname(user.getName());
        String ateId = comment.getParentId();
        if(ateId!=null){
            QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
            commentQueryWrapper.eq("id",ateId);
            Comment one = this.getOne(commentQueryWrapper);
           if(Objects.nonNull(one)){
               if("-1".equals(one.getParentId())){
                   comment.setAte(null);
               }
           }
        }
        boolean save = this.save(comment);
        return save;
    }

    @Override
    @Transactional
    public R<String> delComment(String id) {
        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
        commentQueryWrapper.eq("id",id);
        Comment comment = this.getOne(commentQueryWrapper);
        List<Comment> comments = this.list();
        this.delSonComment(comment, comments);
        System.out.println(ids.toString());
        boolean b = this.removeBatchByIds(ids);
        if(b){
            ids.clear();
            return R.success("删除评论成功");
        }else {
            return R.error("删除评论失败");
        }
    }

    public void delSonComment(Comment comment,List<Comment> comments){
        String id = comment.getId();
        ids.add(id);
         for (int i=0;i< comments.size();i++){
             String parentId = comments.get(i).getParentId();
             if(id.equals(parentId)){
                 Comment node = comments.get(i);
                 this.delSonComment(node,comments);
//                 comments.remove(node);
            }
        }
    }


    public List<Comment> findParent(List<Comment> comments) {
        for (Comment comment : comments) {
            // 防止checkForComodification(),而建立一个新集合
            List<Comment> fatherChildren = new ArrayList<>();
            // 递归处理子级的回复，即回复内有回复
            findChildren(comment, fatherChildren);
            // 将递归处理后的集合放回父级的孩子中
            comment.setCommentList(fatherChildren);
        }
        return comments;
    }

    public void findChildren(Comment parent, List<Comment> fatherChildren) {

        // 找出直接子级
        List<Comment> comments = parent.getCommentList();


        // 遍历直接子级的子级
        for (Comment comment : comments) {
            // 已经到了最底层的嵌套关系，将该回复放入新建立的集合
            fatherChildren.add(comment);
            // 若非空，则还有子级，递归
            if (comment.getCommentList()!=null) {
                findChildren(comment, fatherChildren);
            }
            // 容易忽略的地方：将相对底层的子级放入新建立的集合之后
            // 则表示解除了嵌套关系，对应的其父级的子级应该设为空
            comment.setCommentList(new ArrayList<>());
        }
    }

}
