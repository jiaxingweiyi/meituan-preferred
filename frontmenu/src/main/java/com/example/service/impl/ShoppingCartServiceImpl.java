package com.example.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.ShoppingCart;
import com.example.entity.dtoShoppingCart.DtoShoppingCart;
import com.example.entity.menu.DtoDishSetmealId;
import com.example.entity.menu.VoDishSetmeal;
import com.example.entity.menu.VoFrontCategory;
import com.example.entity.vo.VoShoppingCart;
import com.example.exception.DefineException;
import com.example.mapper.ShoppingCartMapper;
import com.example.service.ShoppingCartService;
import com.example.service.menu.MenuService;
import com.example.utils.Tokenstr;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@Service
@SuppressWarnings("all")
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
     @Autowired
     private MenuService menuService;
    @Override
    public R<List<VoFrontCategory>> menuList() {
        List<VoFrontCategory> voFrontCategories = menuService.frontMenuList();
        return R.success(voFrontCategories);
    }

    @Override
    public R<List<VoDishSetmeal>> dishList(String id,Integer status) {
        List<VoDishSetmeal> voFrontCategories = menuService.frontDishes(id, status);
        return R.success(voFrontCategories);
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<String> addBuyCar(DtoShoppingCart dtoShoppingCart, HttpServletRequest request) {
        String name = dtoShoppingCart.getName();
        String token = Tokenstr.tokenStr(request);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String userId =(String) map.get("id");
        System.out.println(userId);
        DtoDishSetmealId dtoDishSetmealId = menuService.dtoDishSetmealId(name);
        String dishId = dtoDishSetmealId.getDishId();
        QueryWrapper<ShoppingCart> shoppingCartQueryWrapper = new QueryWrapper<>();
        shoppingCartQueryWrapper.eq("is_deleted",0);
        shoppingCartQueryWrapper.eq("name",name);
        shoppingCartQueryWrapper.eq("user_id",userId);
        System.out.println(StringUtils.hasText(dtoShoppingCart.getDishFlavor()));
        if(StringUtils.hasText(dtoShoppingCart.getDishFlavor())){
            shoppingCartQueryWrapper.eq("dish_flavor",dtoShoppingCart.getDishFlavor());
        }
        QueryWrapper<ShoppingCart> countWrapper = new QueryWrapper<>();
        countWrapper.eq("is_deleted",0);
        countWrapper.eq("name",name);
        countWrapper.eq("user_id",userId);
        ShoppingCart shoppingCart = new ShoppingCart();
        ShoppingCart existShoe = this.getOne(shoppingCartQueryWrapper);
        if(Objects.nonNull(existShoe)){
            Integer number = existShoe.getNumber();
            shoppingCart.setNumber(++number);
            boolean update = this.update(shoppingCart, shoppingCartQueryWrapper);
            long count = this.count(countWrapper);
            if(update){
                return R.success(count);
            }
        }
        if(StringUtils.hasText(dishId)&&!"1".equals(dishId)){
            dtoShoppingCart.setDishId(dishId);
        }else {
            String setmealId = dtoDishSetmealId.getSetmealId();
            dtoShoppingCart.setSetmealId(setmealId);
        }
        BeanUtils.copyProperties(dtoShoppingCart,shoppingCart);
        shoppingCart.setUserId(userId);
        boolean save = this.save(shoppingCart);
        if(save){
            long count = this.count(countWrapper);
            return R.success(count);
        }else {
            return R.error("网络开小差");
        }
    }

    @SneakyThrows
    @Override
    public R<VoShoppingCart> shoppingList(HttpServletRequest request) {
        String token = Tokenstr.tokenStr(request);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String userId =(String) map.get("id");
        QueryWrapper<ShoppingCart> shoppingCartQueryWrapper = new QueryWrapper<>();
        shoppingCartQueryWrapper.eq("user_id",userId);
        shoppingCartQueryWrapper.eq("is_deleted",0);
        List<ShoppingCart> shoppingCarts = this.list(shoppingCartQueryWrapper);
        ArrayList<VoShoppingCart> voShoppingCarts = new ArrayList<>();
        for (ShoppingCart shoppingCart : shoppingCarts) {
            VoShoppingCart voShoppingCart = new VoShoppingCart();
            BeanUtils.copyProperties(shoppingCart,voShoppingCart);
            voShoppingCarts.add(voShoppingCart);
        }
        return R.success(voShoppingCarts);
    }

    @SneakyThrows
    @Override
    @Transactional
    public R<Integer> subBuyCar(DtoShoppingCart dtoShoppingCart,HttpServletRequest request) {
        String name = dtoShoppingCart.getName();
        String token = Tokenstr.tokenStr(request);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String userId =(String) map.get("id");
        QueryWrapper<ShoppingCart> shoppingCartQueryWrapper = new QueryWrapper<>();
        shoppingCartQueryWrapper.eq("is_deleted",0);
        shoppingCartQueryWrapper.eq("name",name);
        shoppingCartQueryWrapper.eq("user_id",userId);
        shoppingCartQueryWrapper.eq("dish_flavor",dtoShoppingCart.getDishFlavor());
        ShoppingCart shoppingCart = this.getOne(shoppingCartQueryWrapper);
        Integer number = shoppingCart.getNumber();
        if(number==1){
            boolean remove = this.remove(shoppingCartQueryWrapper);
            if(!remove){
                throw new DefineException("移除菜品失败");
            }else {
                return R.success(0);
            }
        }
        if(Objects.nonNull(shoppingCart)&&number>1){
            shoppingCart.setNumber(--number);
            boolean update = this.update(shoppingCart, shoppingCartQueryWrapper);
            if(!update){
                throw new DefineException("移除菜品失败");
            }
        }
        return R.success(number);
    }

    @Override
    public R<String> clearBuyCar(HttpServletRequest request) {
        String userId = Tokenstr.getId(request);
        QueryWrapper<ShoppingCart> shoppingCartQueryWrapper = new QueryWrapper<>();
        shoppingCartQueryWrapper.eq("user_id",userId);
        boolean remove = this.remove(shoppingCartQueryWrapper);
        if(remove){
            return R.success("清空购物车成功");
        }else {
            return R.error("网络开小差");
        }
    }

    @Override
    public Boolean delBuyCar(String userId) {
        QueryWrapper<ShoppingCart> shoppingWrapper = new QueryWrapper<>();
        shoppingWrapper.eq("user_id",userId);
        shoppingWrapper.eq("is_deleted",false);
        boolean remove = this.remove(shoppingWrapper);
        return remove;
    }

}
