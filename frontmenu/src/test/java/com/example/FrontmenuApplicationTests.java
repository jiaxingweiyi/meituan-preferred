package com.example;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.*;

@SpringBootTest
class FrontmenuApplicationTests {

    public static void main(String[] args) {
        Write write = new Write();
        ProxyTarget proxyTarget = new ProxyTarget(write);
        Do o =(Do) Proxy.newProxyInstance(write.getClass().getClassLoader(), new Class[]{Do.class}, proxyTarget);
        o.doWrite();

    }

}
class ProxyTarget implements InvocationHandler{
    private Write write;

    public ProxyTarget(Write write) {
        this.write = write;
    }



    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("准备纸🖊");
        method.invoke(write,args);
        System.out.println("上交处理");
        return null;
    }
}
class Write implements Do{

    @Override
    public void doWrite() {
        System.out.println("我写完了");
    }
}
interface Do{
    void doWrite();
}
