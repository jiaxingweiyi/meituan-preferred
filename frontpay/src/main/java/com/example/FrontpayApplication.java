package com.example;

import com.example.service.address.AddressService;
import com.example.service.shoppingcart.ShoppingCartService;

import com.example.service.user.UserService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.example.mapper"})
@EnableFeignClients(clients = {AddressService.class, ShoppingCartService.class, UserService.class})
public class FrontpayApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrontpayApplication.class, args);
    }

}
