package com.example.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "wx")
@Configuration
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeChat {
    private String appid;
    private String mahId;
    private String key;
    private String notifyurl;

}
