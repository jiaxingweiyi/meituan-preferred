package com.example.controller;


import com.example.common.R;
import com.example.entity.address.DtoAddress;
import com.example.service.impl.OrderDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单明细表 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/order")
public class OrderDetailController {
    @Autowired
    private OrderDetailServiceImpl orderDetailService;
    @GetMapping("/default")
    public R<DtoAddress> defaultAddress(){
        return orderDetailService.getDefaultAddress();
    }
}

