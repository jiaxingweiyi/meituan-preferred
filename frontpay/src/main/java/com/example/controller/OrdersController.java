package com.example.controller;


import com.alibaba.fastjson.JSON;

import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.Orders;
import com.example.entity.dto.DtoOrder;
import com.example.service.impl.OrdersServiceImpl;
import com.example.service.shoppingcart.ShoppingCartService;
import com.example.utils.Tokenstr;

import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
@RestController
@RequestMapping("/order")
public class OrdersController {
     @Autowired
     private OrdersServiceImpl ordersService;
     @Autowired
     private RabbitTemplate rabbitTemplate;
    @Autowired
    private ShoppingCartService shoppingCartService;
     @PostMapping("/addOrder")
     public R<String> produceOrders(HttpServletRequest request,
                                    @RequestBody DtoOrder dtoOrder){
        return ordersService.produceOrder(request,dtoOrder);
     }
    @PostMapping("/submit/{orderId}")
    public R<Map<String,Object>> order(HttpServletRequest request,
                                       @PathVariable("orderId")String orderId){
        return ordersService.paymentOrder(request,orderId);
    }
    @SneakyThrows
    @GetMapping("/queryPayStatus/{orderId}")
    public R<String> queryPayStatus(@PathVariable("orderId")String orderId,HttpServletRequest request){
        Map<String, String> resultMap = ordersService.queryPaymentStatus(orderId);
        if (resultMap == null) {//出错
            return R.error("支付出错");
        }
        if ("SUCCESS".equals(resultMap.get("trade_state"))) {//如果成功
            String token = Tokenstr.tokenStr(request);
            Map<String, Object> map = JwtConfig.getInfo(token);
            String userId =(String) map.get("id");
            map.put("orderId",orderId);
            String s = JSON.toJSONString(map);
            CorrelationData correlationData = new CorrelationData(s);
            rabbitTemplate.convertAndSend("D","d",s,message -> {
                message.getMessageProperties().setExpiration("900000");
                return message;
            },correlationData);
            Boolean delBuyCar = shoppingCartService.removeBuyCar(userId);
            if(delBuyCar){
                return R.success("支付成功");
            }
        }
        return R.success("支付中");
    }
    @GetMapping("/userPage")
    public R<Map<String,Object>> orderPage(@RequestParam("page")Long page, @RequestParam("pageSize")Long pageSize){
       return ordersService.orders(page,pageSize);
    }
    @GetMapping("/frontpage")
    public String orderList(@RequestParam("page") Long page,
                            @RequestParam("pageSize") Long pageSize,
                            @RequestParam(value = "number",required = false)String number,
                            @RequestParam(value = "beginTime",required = false)String beginTime,
                            @RequestParam(value = "endTime",required = false)String endTime){
        return ordersService.orderList(page,pageSize,number,beginTime,endTime);
    }
    @PutMapping("/frontorders/{id}/{status}")
    public boolean sendStatus(@PathVariable("id")String id
            ,@PathVariable("status")Integer status){
        System.out.println(id);
        System.out.println(status);
        Orders orders = new Orders();
        orders.setStatus(status);
        orders.setId(id);
        boolean b = ordersService.updateById(orders);
        return b;
    }
}

