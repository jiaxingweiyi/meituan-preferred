package com.example.mq;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MQconfig {
    @Bean
    public Queue queueA(){
        Map<String, Object> map = new HashMap<>(1);
        map.put("x-max-length",10000);
        return new Queue("A",false,true,true,map);
    }
    @Bean
    public CustomExchange customExchange(){
        Map<String, Object> map = new HashMap<>(2);
        map.put("x-delayed-type","direct");
        map.put("alternate-exchange","B");
        return new CustomExchange("A","x-delayed-message",false,true,map);
    }
    @Bean
    public Binding bindingA(){
        return BindingBuilder.bind(queueA()).to(customExchange()).with("a").noargs();
    }
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("B",false,true,null);
    }
    @Bean
    public Queue queueB(){
        Map<String, Object> map = new HashMap<>(3);
        map.put("x-dead-letter-exchange","C");
        map.put("x-dead-letter-routing-key","c");
        map.put("x-max-length",20000);
        return new Queue("B",false,true,true,map);
    }
    @Bean
    public Binding bindingB(){
        return BindingBuilder.bind(queueB()).to(fanoutExchange());
    }
    @Bean
    public DirectExchange directExchange(){
        Map<String, Object> map = new HashMap<>(2);
        return new DirectExchange("C",false,true,map);
    }
    @Bean
    public Queue queueC(){
        Map<String, Object> map = new HashMap<>(2);
        map.put("x-max-length",20000);
        return new Queue("C",false,true,true,map);
    }
    @Bean
    public Binding bindingC(){
        return BindingBuilder.bind(queueC()).to(directExchange()).with("c");
    }

    @Bean
    public Queue queueD(){
        Map<String, Object> map = new HashMap<>(2);
        map.put("x-max-length",10000);
        return new Queue("D",false,true,true,map);
    }
    @Bean
    public CustomExchange customExchangeD(){
        Map<String, Object> map = new HashMap<>(2);
        map.put("x-delayed-type","direct");
        map.put("alternate-exchange","B");
        return new CustomExchange("D","x-delayed-message",false,true,map);
    }
    @Bean
    public Binding bindingD(){
        return BindingBuilder.bind(queueD()).to(customExchangeD()).with("d").noargs();
    }
}
