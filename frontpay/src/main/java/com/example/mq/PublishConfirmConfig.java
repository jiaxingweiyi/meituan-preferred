package com.example.mq;

import com.rabbitmq.client.Return;
import com.rabbitmq.client.ReturnCallback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class PublishConfirmConfig implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnsCallback {
     @Autowired
     private RabbitTemplate rabbitTemplate;
    @PostConstruct
    public void init(){
       rabbitTemplate.setConfirmCallback(this);
       rabbitTemplate.setReturnsCallback(this);

    }

    @Override
    public void confirm(CorrelationData correlationData, boolean b, String s) {
        if(!b){
         log.error("消息丢失:"+correlationData.getId());
         log.error("消息丢失:"+s);
        }
    }

    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        String msg = new String(returnedMessage.getMessage().getBody());
        log.error("队列消息丢失:"+msg);
    }
}
