package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.OrderDetail;
import com.example.entity.address.DtoAddress;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface OrderDetailService extends IService<OrderDetail> {
    R<DtoAddress> getDefaultAddress();
}
