package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.R;
import com.example.entity.dto.DtoOrder;
import com.example.entity.Orders;
import com.example.entity.order.VoOrder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author yeweilin
 * @since 2022-05-22
 */
public interface OrdersService extends IService<Orders> {
  R<Map<String,Object>> paymentOrder(HttpServletRequest request,String orderId);
  R<String> produceOrder(HttpServletRequest request,DtoOrder dtoOrder);
  Map<String,String> queryPaymentStatus(String orderId);
  R<Map<String,Object>> orders(Long page, Long pageSize);
  String orderList(Long page, Long pageSize, String number,String beginTime,String endTime);
}
