package com.example.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.OrderDetail;
import com.example.entity.address.DtoAddress;
import com.example.mapper.OrderDetailMapper;
import com.example.service.OrderDetailService;
import com.example.service.address.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail>implements OrderDetailService {
    @Autowired
    private AddressService addressService;
    @Override
    public R<DtoAddress> getDefaultAddress() {
        System.out.println("default");
        ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String tokenStr=request.getHeader("token");
        String token = JSON.parseObject(tokenStr, String.class);
        Map<String, Object> map = JwtConfig.getInfo(token);
        String id =(String) map.get("id");
        DtoAddress defaultAddress = addressService.getDefaultAddress(id);
        return R.success(defaultAddress);
    }
}
