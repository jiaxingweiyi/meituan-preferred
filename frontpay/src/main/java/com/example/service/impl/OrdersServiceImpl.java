package com.example.service.impl;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.R;
import com.example.config.HttpClient;
import com.example.config.WeChat;
import com.example.entity.OrderDetail;
import com.example.entity.Orders;
import com.example.entity.address.VoAddress;
import com.example.entity.dto.DtoOrder;
import com.example.entity.order.VoOrder;
import com.example.entity.user.VoUser;
import com.example.exception.DefineException;
import com.example.mapper.OrdersMapper;
import com.example.service.OrdersService;
import com.example.service.address.AddressService;
import com.example.service.user.UserService;
import com.example.utils.Tokenstr;
import com.github.wxpay.sdk.WXPayUtil;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


/**
 * @author yeweilin
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {
    @Autowired
    private OrderDetailServiceImpl orderDetailService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private PlatformTransactionManager platformTransactionManager;
    @Autowired
    private TransactionDefinition transactionDefinition;
    @Autowired
    private UserService userService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private WeChat weChat;

    @Override
    public R<String> produceOrder(HttpServletRequest request, DtoOrder dtoOrder) {
        String userId = Tokenstr.getId(request);
        String orderId = IdWorker.getIdStr();
        Map<Object, Object> map = new HashMap<>(4);
        map.put("dtoOrder",dtoOrder);
        map.put("userId",userId);
        map.put("orderId",orderId);
        String s = JSON.toJSONString(map);
        CorrelationData correlationData = new CorrelationData(s);
        rabbitTemplate.convertAndSend("A","a",map,message -> {
            message.getMessageProperties().setExpiration("900000");
            return message;
        },correlationData);
        return R.success(orderId);
    }

    @SneakyThrows
    @Override
    public R<Map<String,Object>> paymentOrder(HttpServletRequest request,String orderId) {
        //调用微信接口返回二维码
        try {
            //2 调用微信接口，得到二维码地址等信息
            //封装传递微信地址参数
            Map paramMap = new HashMap();
            paramMap.put("appid", weChat.getAppid());
            paramMap.put("mch_id", weChat.getMahId());
            paramMap.put("nonce_str", WXPayUtil.generateNonceStr());

            String body = "订单号"+orderId;
            paramMap.put("body", body);//扫码后手机显示内容

            paramMap.put("out_trade_no",orderId);
            paramMap.put("total_fee", "1");
            paramMap.put("spbill_create_ip", "127.0.0.1");
            paramMap.put("notify_url", weChat.getNotifyurl());
            paramMap.put("trade_type", "NATIVE");
            //请求微信生成二维码接口
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            //设置post请求相关参数
            //微信支付要求传递参数xml格式
            //把封装map集合变成xml，加密处理，传输
            String xml = WXPayUtil.generateSignedXml(paramMap, weChat.getKey());
            client.setXmlParam(xml);
            //支持https协议
            client.setHttps(true);
            //发送
            client.post();

            //调用微信接口，返回数据,xml格式的数据
            String resultXml = client.getContent();
            System.out.println("微信二维码："+resultXml);
            //把xml格式数据转换map
            Map<String, String> resultMap = WXPayUtil.xmlToMap(resultXml);
            Map map = new HashMap<>();
            map.put("orderId", orderId);
            map.put("totalFee", "1");
            map.put("resultCode", resultMap.get("result_code"));
            map.put("codeUrl", resultMap.get("code_url"));
            return R.success(map);
        } catch (Exception e) {
            log.error("生成二维码失败");
        }
        throw new DefineException("生成二维码失败");
    }
   @Override
   @SneakyThrows
    public Map<String,String> queryPaymentStatus(String orderId) {
        try {
            //2 封装微信接口需要数据
            Map paramMap = new HashMap<>(8);
            paramMap.put("appid", weChat.getAppid());
            paramMap.put("mch_id", weChat.getMahId());
            paramMap.put("out_trade_no", orderId);
            paramMap.put("nonce_str", WXPayUtil.generateNonceStr());

            //3 调用微信接口，传递数据，设置参数
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            client.setXmlParam(WXPayUtil.generateSignedXml(paramMap,weChat.getKey()));
            client.setHttps(true);
            client.post();

            //4 获取微信接口返回数据
            String xml = client.getContent();
            System.out.println("支付状态返回xml: "+xml);
            Map<String, String> resultMap = WXPayUtil.xmlToMap(xml);

            return resultMap;

        } catch (Exception e) {
            e.printStackTrace();
            throw new DefineException("查询失败");
        }
    }

    @Override
    public R<Map<String,Object>> orders(Long page, Long pageSize) {
        Page<Orders> ordersPage = this.page(new Page<Orders>(page, pageSize));;
        List<Orders> records = ordersPage.getRecords();
        for (Orders record : records) {
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq("order_id",record.getNumber());
            long count = orderDetailService.count(orderDetailQueryWrapper);
            record.setSumNum(count);
        }
        long pages = ordersPage.getPages();
        long number = ordersPage.getTotal();
        Map<String, Object> map = new HashMap<>(4);
        map.put("records",records);
        map.put("pages",pages);
        map.put("number",number);
        return R.success(map);
    }

    @Override
    public String orderList(Long page,Long pageSize,String number,String beginTime,String endTime) {
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        LocalDateTime begin=null;
        LocalDateTime end=null;
        ordersQueryWrapper.eq(Objects.nonNull(number),"number",number);
        if((!StringUtils.isBlank(beginTime)||!StringUtils.isBlank(endTime))){
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
             begin=LocalDateTime.parse(beginTime,dateTimeFormatter);
             end=LocalDateTime.parse(endTime,dateTimeFormatter);
        }
        ordersQueryWrapper.between((!StringUtils.isBlank(beginTime)||!StringUtils.isBlank(endTime)),"order_time",
                begin,end);
        Page<Orders> orderPage = this.page(new Page<Orders>(page,pageSize),ordersQueryWrapper);
        List<Orders> records = orderPage.getRecords();
        List<VoOrder> voOrders = new ArrayList<>();
        Map<String, Object> map = new HashMap<>(4);
        for (Orders record : records) {
            String userId = record.getUserId();
            VoOrder voOrder = new VoOrder();
            BeanUtils.copyProperties(record,voOrder);
            VoUser user = userService.user(userId);
            if(Objects.nonNull(user)){
                voOrder.setPhone(user.getPhone());
            }
            VoAddress voAddress = addressService.byIdAddress(userId);
            if(Objects.nonNull(voAddress)){
                voOrder.setUserName(voAddress.getConsignee());
                voOrder.setAddress(voAddress.getDetail());
            }
            voOrders.add(voOrder);
        }
        long total = orderPage.getTotal();
        long size = orderPage.getSize();
        map.put("records",voOrders);
        map.put("total",total);
        map.put("size",size);
        String mapStr = JSON.toJSONString(map);
        return mapStr;
    }


    @SneakyThrows
    @Transactional
    @RabbitListener(queues = {"A"},concurrency = "3")
    public void consume(Message message, Channel channel, Map map){
//        生成订单队列
        TransactionStatus transaction = platformTransactionManager.getTransaction(transactionDefinition);
        String orderId =(String) map.get("orderId");;
        DtoOrder dtoOrder =(DtoOrder) map.get("dtoOrder");
        String userId =(String) map.get("userId");
        Orders orders = new Orders();
        BeanUtils.copyProperties(dtoOrder, orders);
        orders.setOrderTime(LocalDateTime.now());
        String userName = dtoOrder.getConsignee();
        orders.setNumber(orderId);
        orders.setUserName(userName);
        orders.setUserId(userId);
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.eq("number",orderId);
        Orders existOrder = this.getOne(ordersQueryWrapper);
        if(Objects.nonNull(existOrder)){
            return;
        }
        boolean save = this.save(orders);
            if(save){
                List<OrderDetail> orderDetails = dtoOrder.getOrderDetails();
                for (OrderDetail orderDetail : orderDetails) {
                    orderDetail.setId(IdWorker.getIdStr());
                    orderDetail.setOrderId(orderId);
                }
                boolean b = orderDetailService.saveBatch(orderDetails);
                if(b){
                   channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
                    platformTransactionManager.commit(transaction);
                    return;
                }
            }
                channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,false);
                platformTransactionManager.rollback(transaction);
    }
    @SneakyThrows
    @RabbitListener(queues = {"D"})
    @Transactional
    public void delPayOverMsg(Message message, Channel channel,String s){
//         下单成功后删除购物车信息和更新订单信息
        TransactionStatus transaction = platformTransactionManager.getTransaction(transactionDefinition);
        Map map = JSON.parseObject(s, Map.class);
        String orderId =(String) map.get("orderId");
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.eq("number",orderId);
        Orders orders = new Orders();
        orders.setStatus(2);
        orders.setCheckoutTime(LocalDateTime.now());
        boolean update = this.update(orders, ordersQueryWrapper);
        if(update){
                platformTransactionManager.commit(transaction);
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
                return;
        }
        platformTransactionManager.rollback(transaction);
        channel.basicNack(message.getMessageProperties().getDeliveryTag(),false,false);
    }

}
