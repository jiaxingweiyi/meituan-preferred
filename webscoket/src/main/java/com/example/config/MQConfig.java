package com.example.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MQConfig {
    @Bean
    public Queue sessionQueue(){
        Map<String, Object> map = new HashMap<>(4);
        return new Queue("sessionQueue",false,false,true,map);
    }
    @Bean
    public FanoutExchange sessionFanout(){
        Map<String, Object> map = new HashMap<>(4);
        return new FanoutExchange("sessionFanout",false,true,map);
    }
    @Bean
    public Binding bindingBuilderSession(){
        return BindingBuilder.bind(sessionQueue()).to(sessionFanout());
    }
}
