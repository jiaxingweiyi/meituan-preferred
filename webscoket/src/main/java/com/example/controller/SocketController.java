package com.example.controller;


import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.WechatInfo;
import com.example.entity.user.VoUser;
import com.example.service.SocketServiceImpl;
import com.example.service.user.UserService;
import com.example.utils.Tokenstr;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author yeweilin
 */

@RestController
@RequestMapping("/socket")
public class SocketController {
    private static final Logger logger = LoggerFactory.getLogger(SocketController.class);
    @Autowired
    private SocketServiceImpl socketService;
    @Autowired
    private UserService userService;
    @SneakyThrows
    @GetMapping("/friend")
    public R<List<VoUser>> friendList(HttpServletRequest request,
                                      @RequestParam(value = "phone",required = false) String phone) {
        Map<String, Object> userMap = JwtConfig.getInfo(Tokenstr.tokenStr(request));
        String CurrentUserPhone =(String) userMap.get("username");
        List<VoUser> users = userService.users(CurrentUserPhone,phone);
        return R.success(users);
    }
    @GetMapping("/sendFriend/{phone}")
    public R<Boolean> sendFriendRequest(HttpServletRequest request,
                                        @PathVariable("phone") String phone){
        return socketService.sendFriendRequest(request,phone);
    }
    @GetMapping("/friends")
    public R<List<VoUser>> users(HttpServletRequest request){
        return socketService.userList(request);
    }

    @GetMapping("/request")
    public R<Set<String>> requestUser(HttpServletRequest request){
        return socketService.friendAddReq(request);
    }
    @GetMapping("/ifagree/{phone}/{agree}")
    public R<String> ifAgree(@PathVariable("phone") String phone,
                             @PathVariable("agree") boolean agree, HttpServletRequest request){

        return socketService.ifAgree(phone,agree,request);
    }
    @GetMapping("/wechatinfo/{from}/{to}")
    public R<List<WechatInfo>> userWechatInfo(@PathVariable("from")String from,
                                              @PathVariable("to")String to){
        return socketService.wechatInfo(from,to);
    }
}
