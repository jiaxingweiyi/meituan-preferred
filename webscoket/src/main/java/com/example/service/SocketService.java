package com.example.service;

import com.example.common.R;
import com.example.entity.WechatInfo;
import com.example.entity.user.VoUser;


import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;


/**
 * @author yeweilin
 */
public interface SocketService {
     R<Boolean> sendFriendRequest(HttpServletRequest request,String phone);
     R<List<VoUser>> userList(HttpServletRequest request);
     R<Set<String>> friendAddReq(HttpServletRequest request);
     R<List<WechatInfo>> wechatInfo(String fromName,String toName);
}
