package com.example.service;


import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.WechatInfo;
import com.example.entity.user.VoUser;
import com.example.service.user.UserService;
import com.example.utils.Tokenstr;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SocketServiceImpl implements SocketService {
    @Autowired
    @Qualifier("redis")
   private RedisTemplate redisTemplate;
    @Autowired
    private UserService userService;
    @SneakyThrows
    @Override
    public R<Boolean> sendFriendRequest(HttpServletRequest request, String phone) {
//        将用户好友申请请求放入redis中
        Map<String, Object> userMap = JwtConfig.getInfo(Tokenstr.tokenStr(request));
        String currentUserPhone =(String) userMap.get("username");
        Boolean sendRequest = redisTemplate.opsForZSet().add("unfriend:"+phone,currentUserPhone,System.currentTimeMillis());
        if(sendRequest){
            return R.success("发送好友申请成功");
        }else {
            return R.success("发送好友申请失败");
        }
    }

    @SneakyThrows
    @Override
    public R<List<VoUser>> userList(HttpServletRequest request) {
        Map<String, Object> userMap = JwtConfig.getInfo(Tokenstr.tokenStr(request));
        String currentUserPhone =(String) userMap.get("username");
        Set<String> set = redisTemplate.opsForZSet().range("friend:" + currentUserPhone, 0, System.currentTimeMillis());
        int size = set.size();
        if(size==0){
            return R.error("没有好友");
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String phone : set) {
         stringBuffer.append(phone).append(",");
        }
        String sub = stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1);
        R success = R.success(userService.byPhoneUser(sub));
        return success;
    }

    @SneakyThrows
    @Override
    public R<Set<String>> friendAddReq(HttpServletRequest request) {
        Map<String, Object> userMap = JwtConfig.getInfo(Tokenstr.tokenStr(request));
        String currentUserPhone =(String) userMap.get("username");
        Set<String> set = redisTemplate.opsForZSet().range("unfriend:" + currentUserPhone, 0, System.currentTimeMillis());
        return R.success(set);
    }

//    显示对应用户的聊天信息
    @Override
    public R<List<WechatInfo>> wechatInfo(String fromName,String toName) {
        System.out.println(fromName);
        System.out.println(toName);
        List<WechatInfo> allInfo = redisTemplate.opsForList().range("wechat:" + fromName + ":" + "to:" + toName, 0, -1);
        List<WechatInfo> friList = redisTemplate.opsForList().range("wechat:" + toName + ":" + "to:" + fromName, 0, -1);
        System.out.println(allInfo);
        System.out.println(friList);
        allInfo.addAll(friList);
         allInfo = allInfo.stream().sorted((s, s1) -> {
            Long front = s.getTime();
            Long back = s1.getTime();
            if (front > back) {
                return 1;
            } else {
                return -1;
            }
        }).collect(Collectors.toList());
       return R.success(allInfo);
    }

    @SneakyThrows
    public R<String> ifAgree(String phone, boolean agree, HttpServletRequest request){
        Map<String, Object> userMap = JwtConfig.getInfo(Tokenstr.tokenStr(request));
        String currentUserPhone =(String) userMap.get("username");
        if(agree){
            Long remove = redisTemplate.opsForZSet().remove("unfriend:" + currentUserPhone, phone);
            System.out.println(remove);
            if(remove<=0){
                return R.error("网络异常");
            }
            agree = redisTemplate.opsForZSet().add("friend:" + currentUserPhone, phone, System.currentTimeMillis());
            boolean agree1 = redisTemplate.opsForZSet().add("friend:" + phone,currentUserPhone, System.currentTimeMillis());
            if(agree&&agree1){
                return R.success("添加成功");
            }else {
                
                return R.error("网络异常");
            }
        }else {
          Long  remove = redisTemplate.opsForZSet().remove("unfriend:" + currentUserPhone, phone);
        }
            return R.success("拒绝成功");
    }
}