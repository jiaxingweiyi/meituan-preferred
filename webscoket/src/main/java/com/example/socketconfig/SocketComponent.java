package com.example.socketconfig;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import com.alibaba.fastjson.JSON;
import com.example.config.SpringUtil;
import com.example.entity.WechatInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author websocket服务
 */
@Component
@ServerEndpoint("/socket/web/{username}")
public class SocketComponent{

    private static final Logger log = LoggerFactory.getLogger(SocketComponent.class);
    /**
     * 记录当前在线连接数
     */
    public static final Map<String, RemoteEndpoint.Basic> sessionMap = new ConcurrentHashMap<>();

    /**
     * 连接建立成功调用的方法
     */


    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        RemoteEndpoint.Basic basicRemote = session.getBasicRemote();
        sessionMap.put(username,basicRemote);
        log.info("有新用户加入，username={}, 当前在线人数为：{}", username, sessionMap.size());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session, @PathParam("username") String username) {
        sessionMap.remove(username);
        log.info("有一连接关闭，移除username={}的用户session, 当前在线人数为：{}", username, sessionMap.size());
    }



    /**
     * 收到客户端消息后调用的方法
     * 后台收到客户端发送过来的消息
     * onMessage 是一个消息的中转站
     * 接受 浏览器端 socket.send 发送过来的 json数据
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session, @PathParam("username") String username) {
        log.info("服务端收到用户username={}的消息:{}", username, message);
        RedisTemplate<String,Object> redisTemplate
                =(RedisTemplate) SpringUtil.getBean("redisTemplate1");
        JSONObject obj = JSONUtil.parseObj(message);
        String toUsername = obj.getStr("to"); // to表示发送给哪个用户，比如 admin
        String text = obj.getStr("text"); // 发送的消息文本  hello
        WechatInfo wechatInfo = new WechatInfo();
        wechatInfo.setTime(System.currentTimeMillis());
        wechatInfo.setName(username);
        wechatInfo.setMsg(text);
        // {"to": "admin", "text": "聊天文本"}
        // 根据 to用户名来获取 session，再通过session发送消息文本
        RemoteEndpoint.Basic toSession =(RemoteEndpoint.Basic) sessionMap.get(toUsername);
        if(Objects.isNull(toSession)){
            redisTemplate.opsForList().leftPush("wechat:"+username+":to:"+toUsername,wechatInfo);
            return;
        }
            // {"from": "zhang", "text": "hello"}
            JSONObject jsonObject = new JSONObject();
        // from 是 zhang
            jsonObject.set("from", username);
        // text 同上面的text
            jsonObject.set("text", text);
        redisTemplate.opsForList().leftPush("wechat:"+username+":to:"+toUsername,wechatInfo);
            this.sendMessage(jsonObject.toString(), toSession);
            log.info("发送给用户username={}，消息：{}", toUsername, jsonObject.toString());

    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, RemoteEndpoint.Basic toSession) {
        try {
            log.info("服务端给客户端[{}]发送消息{}", toSession.toString(), message);
            toSession.sendText(message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败", e);
        }
    }
}

