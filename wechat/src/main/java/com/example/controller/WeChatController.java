package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.common.JwtConfig;
import com.example.common.R;
import com.example.entity.WeChat;
import com.example.entity.user.DtoUser;
import com.example.exception.DefineException;
import com.example.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yeweilin
 */
@Controller
@RequestMapping("/wx")
public class WeChatController {
    @Autowired
    private WeChat weChat;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private UserService userService;
    @GetMapping("/login")
    @ResponseBody
    public R<Map<String, Object>> wechatLogin(){
        Map<String, Object> map = new HashMap<>(4);
        map.put("appid",weChat.getAppid());
        map.put("redirect_uri",weChat.getRedirecturl());
        map.put("state","state");
        System.out.println(map.toString());
        return R.success(map);
    }

    @GetMapping("/callBack")
    public String callbackWechat(@RequestParam("code")String code
            , @RequestParam("state")String state, HttpServletResponse response){
        String requestUrl="https://api.weixin.qq.com/sns/oauth2/access_token?" +
                "appid=%s" +
                "&secret=%s" +
                "&code=%s" +
                "&grant_type=authorization_code";
        String url = String.format(requestUrl, weChat.getAppid(), weChat.getAppsecret(), code);
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        String body = entity.getBody();
        JSONObject jsonObject = JSON.parseObject(body);
        String accessToken = (String) jsonObject.get("access_token");
        String refreshToken =(String) jsonObject.get("refresh_token");
        String openId =(String) jsonObject.get("openid");
        String accessUrl = "https://api.weixin.qq.com/sns/userinfo?" +
                "access_token=%s"+
                "&grant_type=%s"+
                "&openid=%s";
        accessUrl=String.format(accessUrl,accessToken, refreshToken,openId);
        ResponseEntity<String> tokenEntity = restTemplate.getForEntity(accessUrl, String.class);
        String body1 = tokenEntity.getBody();
        JSONObject wechatInfo = JSON.parseObject(body1);
        String headUrl =(String) wechatInfo.get("headimgurl");
        String nickname =(String) wechatInfo.get("nickname");
        Integer sex =(Integer) wechatInfo.get("sex");
        String openid =(String) wechatInfo.get("openid");
        DtoUser dtoUser = new DtoUser();
        dtoUser.setPhone(openid.substring(0,19));
        dtoUser.setId(openid.substring(0,19));
        dtoUser.setAvatar(headUrl);
        dtoUser.setSex(sex.toString());
        dtoUser.setName(nickname);
        System.out.println(nickname);
        byte[] bytes = nickname.getBytes(StandardCharsets.UTF_8);
        System.out.println(new String(bytes));
        Boolean res = userService.wechatLogin(dtoUser);
        if(!res){
                throw new DefineException("微信登录失败");
        }
        String token = JwtConfig.createToken(openId.substring(0,19), openId.substring(0,19));
        System.out.println(token);
        String redirectUrl="http://localhost/static/front/index.html?token="+token+"&name="+openId.substring(0,19);
        return "redirect:"+redirectUrl;
    }
}
