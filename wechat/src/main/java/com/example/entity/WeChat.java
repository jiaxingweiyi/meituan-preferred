package com.example.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "wx.open")
@Configuration
public class WeChat {
    private String appid;
    private String appsecret;
    private String redirecturl;

    public WeChat() {
    }

    public WeChat(String appid, String appsecret, String redirecturl) {
        this.appid = appid;
        this.appsecret = appsecret;
        this.redirecturl = redirecturl;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }

    public String getRedirecturl() {
        return redirecturl;
    }

    public void setRedirecturl(String redirecturl) {
        this.redirecturl = redirecturl;
    }

    @Override
    public String toString() {
        return "WeChat{" +
                "appid='" + appid + '\'' +
                ", appsecret='" + appsecret + '\'' +
                ", redirecturl='" + redirecturl + '\'' +
                '}';
    }
}
